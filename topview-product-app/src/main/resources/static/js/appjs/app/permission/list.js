$(document).ready(function () {
	load();
});

function load(){
    $('#table-entity').bootstrapTreeTable({
        id: 'id',
        code: 'id',
        parentCode: 'parentId',
        type: "GET", // 请求数据的ajax类型
        url: ctx + 'a/permission/list', // 请求数据的ajax的url
        ajaxParams: {sort: 'sort'}, // 请求数据的ajax的data属性
        expandColumn: '0',// 在哪一列上面显示展开按钮
        striped: true, // 是否各行渐变色
        bordered: true, // 是否显示边框
        expandAll: false, // 是否全部展开
        columns: [
            {
                title: '菜单名',
                valign: 'center',
                field: 'name',
                width: '20%'
            },
            {
                title: '菜单标识',
                valign: 'center',
                width : '20%',
                field: 'keyFun'
            },
            {
                title: '排序',
                valign: 'center',
                width : '20%',
                field: 'sort'
            },
            {
                title: '类型',
                field: 'type',
                align: 'center',
                valign: 'center',
                width : '10%',
                formatter: function (item, index) {
                    if (item.type === '0') {
                        return '<span class="label label-primary">tab</span>';
                    }
                    if (item.type === '1') {
                        return '<span class="label label-success">smallTab</span>';
                    }
                    if (item.type === '2') {
                        return '<span class="label label-warning">mainTab</span>';
                    }
                }
            },
            {
                title: '权限状态',
                field: 'permissionStatus',
                align: 'center',
                valign: 'center',
                width : '20%',
                formatter: function (item, index) {
                    if (item.permissionStatus === '0') {
                        return '<span class="label label-primary">全部不可见</span>';
                    }
                    if (item.permissionStatus === '1') {
                        return '<span class="label label-success">全部可见</span>';
                    }
                    if (item.permissionStatus === '2') {
                        return '<span class="label label-warning">部分可见</span>';
                    }
                }
            },
            {
                title: '操作',
                field: 'id',
                align: 'center',
                valign: 'center',
                width : '20%',
                formatter: function (item, index) {
                    var e = '<button class="btn btn-primary btn-sm s_edit_h" title="编辑" onclick="edit(\''
                        + item.id + '\')"><i class="fa fa-edit"></i></button> ';
                    var d = '<button class="btn btn-warning btn-sm s_remove_h" title="删除" onclick="remove(\''
                        + item.id + '\')"><i class="fa fa-remove"></i></button> ';
                    var p = '<button class="btn btn-primary btn-sm s_add_h" title="添加下级" onclick="add(\''
                        + item.id + '\')"><i class="fa fa-plus"></i></button> ';                    
                    return e + d + p;
                }
            }        
        ]
    });	
}

function reLoad() {
	load();
}

function add(id) {
    layer.open({
        type : 2,
        title : '添加',
        offset: "30px",
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '800px', '540px' ],
        content : ctx + 'a/permission/add/'+id
    });
}

function edit(id) {
    layer.open({
        type : 2,
        title : '编辑',
        offset: "30px",
        maxmin : true,
        shadeClose : false,
        area : [ '800px', '540px' ],
        content : ctx + 'a/permission/edit/'+id
    });
}

function remove(id) {
    layer.confirm('确定要删除选中的记录？', {
        btn: ['确定', '取消'],
        offset: "30px",
    }, function () {
        $.ajax({
            url: ctx + 'a/permission/remove',
            type: "post",
            data: {
                'id': id
            },
            success: function (data) {
                if (data.result == "success") {
                    layer.msg("删除成功");
                    reLoad();
                } else {
                    layer.msg("删除失败");
                }
            }
        });
    })
}

function batchRemove() {
    layer.confirm('确定要删除选中的记录？', {
        btn: ['确定', '取消'],
        offset: "30px",
    }, function () {
    	layer.msg("删除成功");
    	console.log("不支持批量删除操作");
    	reLoad();
    })
}
