$(document).ready(function () {
	load();
});

function load(){
    $('#table-entity').bootstrapTable({
        method : 'get', // 服务器数据的请求方式 get or post
        url: ctx + 'a/updateManager/list', // 请求数据的ajax的url
        striped : true, // 设置为true会有隔行变色效果
        iconSize : 'outline',
        dataType : "json", // 服务器返回的数据类型
        pagination : true, // 设置为true会在底部显示分页条
        singleSelect : false, // 设置为true将禁止多选
        pageSize : 10, // 如果设置了分页，每页数据条数
        pageNumber : 1, // 如果设置了分布，首页页码
        showColumns : false, // 是否显示内容下拉框（选择显示的列）
        sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
        queryParams : function(params) {
            return {
                // 说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                limit : params.limit,
                offset : params.offset,
                name : $('#name').val()
            };
        },
        columns: [
			{
                title: '应用名称 ',
                field: 'name',
                align: 'center',
                valign: 'center',
                width: '10%'
            },
            {
                title: '版本名称',
                field: 'versionName',
                align: 'center',
                valign: 'center',
                width : '10%',
            },
            {
            	title: '版本号',
            	field: 'versionCode',
            	align: 'center',
            	valign: 'center',
            	width : '10%',
            },
            {
            	title: '应用包大小',
            	field: 'size',
            	align: 'center',
            	valign: 'center',
            	width : '10%',
            },
            {
                title: '应用类型',
                field: 'type',
                align: 'center',
                valign: 'center',
                width : '10%',
                formatter: function (item, index) {
                    if (item === '0') {
                        return '<span class="label label-primary">ios</span>';
                    }
                    if (item === '1') {
                        return '<span class="label label-success">android</span>';
                    }
                }
            },
            {
            	title: '应用状态',
            	field: 'status',
            	align: 'center',
            	valign: 'center',
            	width : '10%',
            	formatter: function (item, index) {
            		if (item === '0') {
            			return '<span class="label label-primary">test</span>';
            		}
            		if (item === '1') {
            			return '<span class="label label-success">dev</span>';
            		}
            		if (item === '2') {
            			return '<span class="label label-success">prod</span>';
            		}
            	}
            },
            {
            	title: '是否强制更新',
            	field: 'forceFlag',
            	align: 'center',
            	valign: 'center',
            	width : '10%',
            	formatter: function (item, index) {
            		if (item === '0') {
            			return '<span class="label label-primary">是</span>';
            		}
            		if (item === '1') {
            			return '<span class="label label-success">否</span>';
            		}
            	}
            },
            {
                title: '操作',
                field: 'id',
                align: 'center',
                valign: 'center',
                width : '10%',
                formatter: function (item, index) {
                    var e = '<button class="btn btn-primary btn-sm s_edit_h" title="编辑" onclick="edit(\''
                        + item + '\')"><i class="fa fa-edit"></i></button> ';
                    return e;
                }
            }        
        ]
    });	
}

function reLoad() {
	$('#table-entity').bootstrapTable('refresh');
}

function add(id) {
    layer.open({
        type : 2,
        title : '添加',
        offset: "30px",
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '800px', '540px' ],
        content : ctx + 'a/updateManager/add'
    });
}

function edit(id) {
    layer.open({
        type : 2,
        title : '编辑',
        offset: "30px",
        maxmin : true,
        shadeClose : false,
        area : [ '800px', '540px' ],
        content : ctx + 'a/updateManager/edit/'+id
    });
}

