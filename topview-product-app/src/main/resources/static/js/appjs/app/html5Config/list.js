$(document).ready(function () {
	load();
});

function load(){
    $('#table-entity').bootstrapTable({
        method : 'get', // 服务器数据的请求方式 get or post
        url: ctx + 'a/html5Config/list', // 请求数据的ajax的url
        striped : true, // 设置为true会有隔行变色效果
        iconSize : 'outline',
        dataType : "json", // 服务器返回的数据类型
        pagination : true, // 设置为true会在底部显示分页条
        singleSelect : false, // 设置为true将禁止多选
        pageSize : 10, // 如果设置了分页，每页数据条数
        pageNumber : 1, // 如果设置了分布，首页页码
        showColumns : false, // 是否显示内容下拉框（选择显示的列）
        sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
        queryParams : function(params) {
            return {
                // 说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                limit : params.limit,
                offset : params.offset,
                name : $('#searchName').val()
            };
        },
        columns: [
			{
                title: '名称',
                field: 'name',
                align: 'center',
                valign: 'center',
                width: '10%'
            },
            {
                title: '标识',
                field: 'keyFun',
                align: 'center',
                valign: 'center',
                width : '10%',
            },
            {
                title: '是否滑动',
                field: 'scrollEnabled',
                align: 'center',
                valign: 'center',
                width : '10%',
                formatter: function (item, index) {
                    if (item === '0') {
                        return '<span class="label label-primary">是</span>';
                    }
                    if (item === '1') {
                        return '<span class="label label-success">否</span>';
                    }
                }
            },
            {
            	title: '是否缩放',
            	field: 'scalesPageFit',
            	align: 'center',
            	valign: 'center',
            	width : '10%',
            	formatter: function (item, index) {
            		if (item === '0') {
            			return '<span class="label label-primary">是</span>';
            		}
            		if (item === '1') {
            			return '<span class="label label-success">否</span>';
            		}
            	}
            },
            {
                title: '操作',
                field: 'id',
                align: 'center',
                valign: 'center',
                width : '10%',
                formatter: function (item, index) {
                    var e = '<button class="btn btn-primary btn-sm s_edit_h" title="编辑" onclick="edit(\''
                        + item + '\')"><i class="fa fa-edit"></i></button> ';
                    var d = '<button class="btn btn-warning btn-sm s_remove_h" title="删除" onclick="remove(\''
                        + item + '\')"><i class="fa fa-remove"></i></button> ';
                    return e + d;
                }
            }        
        ]
    });	
}

function reLoad() {
	$('#table-entity').bootstrapTable('refresh');
}

function add(id) {
    layer.open({
        type : 2,
        title : '添加',
        offset: "30px",
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '800px', '540px' ],
        content : ctx + 'a/html5Config/add'
    });
}

function edit(id) {
    layer.open({
        type : 2,
        title : '编辑',
        offset: "30px",
        maxmin : true,
        shadeClose : false,
        area : [ '800px', '540px' ],
        content : ctx + 'a/html5Config/edit/'+id
    });
}

function remove(id) {
    layer.confirm('确定要删除选中的记录？', {
        btn: ['确定', '取消'],
        offset: "30px",
    }, function () {
        $.ajax({
            url: ctx + 'a/html5Config/remove',
            type: "post",
            data: {
                'id': id
            },
            success: function (data) {
                if (data.result == "success") {
                    layer.msg("删除成功");
                    reLoad();
                } else {
                    layer.msg("删除失败");
                }
            }
        });
    })
}

function batchRemove() {
    layer.confirm('确定要删除选中的记录？', {
        btn: ['确定', '取消'],
        offset: "30px",
    }, function () {
    	layer.msg("删除成功");
    	console.log("不支持批量删除操作");
    	reLoad();
    })
}
