$(document).ready(function () {
	var permissions = {};
	
	load();
});

function load(){
    $('#table-entity').bootstrapTable({
        method : 'get', // 服务器数据的请求方式 get or post
        url: ctx + 'a/userBehaviorFilter/list', // 请求数据的ajax的url
        striped : true, // 设置为true会有隔行变色效果
        iconSize : 'outline',
        dataType : "json", // 服务器返回的数据类型
        singleSelect : false, // 设置为true将禁止多选
        pageSize : 10, // 如果设置了分页，每页数据条数
        pageNumber : 1, // 如果设置了分布，首页页码
        showColumns : false, // 是否显示内容下拉框（选择显示的列）
        sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
        queryParams : function(params) {
            return {
            	limit : params.limit,
                offset : params.offset,
                loginName : $('#loginName').val()
            };
        },
        columns: [
			{
                title: '用户',
                field: 'username',
                align: 'center',
                valign: 'center',
                width: '10%'
            },
            {
                title: '更新时间',
                field: 'createTime',
                align: 'center',
                valign: 'center',
                width : '10%',
            },
            {
                title: '操作',
                field: 'id',
                align: 'center',
                valign: 'center',
                width : '10%',
                formatter: function (item, index) {
                    var d = '<button class="btn btn-warning btn-sm s_remove_h" title="删除" onclick="remove(\''
                        + item + '\')"><i class="fa fa-remove"></i></button> ';
                    return d;
                }
            }        
        ]
    });	
}

function reLoad() {
	$('#table-entity').bootstrapTable('refresh');
}

function remove(id) {
    layer.confirm('确定要删除选中的记录？', {
        btn: ['确定', '取消'],
        offset: "30px",
    }, function () {
        $.ajax({
            url: ctx + 'a/userBehaviorFilter/remove',
            type: "post",
            data: {
                'id': id
            },
            success: function (data) {
                if (data.result == "success") {
                    layer.msg("删除成功");
                    reLoad();
                } else {
                    layer.msg("删除失败");
                }
            }
        });
    })
}

function addFilterUser(){
	permissions = {
		permissionUser: getArray(),
	}
	
    layer.open({
        type : 2,
        title : '添加',
        offset: "50px",
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '600px', '500px' ],
        btn: ['确定'],
        yes: function(index, layero){
        	$('#permissionContent').empty();
        	
        	var iframeWindow = window["layui-layer-iframe" + index];
        	var array = iframeWindow.getPermission();
        	
        	var permissionUserIds = array[0];
        	var permissionUserNames = array[1];
        	var puids = Array.from(permissionUserIds);
        	var punas = Array.from(permissionUserNames);
        	puids = puids.toString() != '' ? puids.toString() + "," : "";
        	punas = punas.toString() != '' ? punas.toString() + "," : "";
        	
            $.ajax({
                cache : false,
                type : "POST",
                url : ctx + "a/userBehaviorFilter/save",
                data : {userIds: puids, usernames: punas},// 你的formid
                async : false,
                error : function(request) {layer.alert("Connection error", {offset: "100px"});},
                success : function(data) {
                    if (data.result == "success") {
                    	layer.close(index);
                    	reLoad();
                    } else {
                        layer.alert(data.msg, {offset: "100px"})
                    }
                }
            });
        },
        content : ctx + 'a/permissionManager/users'
    });
}

function getArray(){
	var array = new Array();
	var allData = $('#table-entity').bootstrapTable('getData');
	for(let i = 0; i < allData.length; i ++){
		array.push({id: allData[i].userId, name: allData[i].username, type: '2'});
	}
	return array;
}