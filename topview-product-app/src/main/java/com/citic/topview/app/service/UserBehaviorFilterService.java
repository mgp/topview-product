package com.citic.topview.app.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.app.entity.UserBehaviorFilter;
import com.citic.topview.common.app.vo.UserBehaviorFilterVO;
import com.citic.topview.common.util.IdGen;
import com.citic.topview.system.persistence.CrudService;
import com.citic.topview.system.util.ShiroUtils;
import com.citic.topview.app.dao.UserBehaviorFilterDao;

@Service
@Transactional(readOnly = true)
public class UserBehaviorFilterService extends CrudService<UserBehaviorFilterDao, UserBehaviorFilter>{

	public UserBehaviorFilter get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(UserBehaviorFilter userBehaviorFilter) {
		return super.save(userBehaviorFilter);
	}

	@Transactional(readOnly = false)
	public int update(UserBehaviorFilter userBehaviorFilter) {
		return super.update(userBehaviorFilter);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<UserBehaviorFilterVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<UserBehaviorFilterVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}

	@Transactional(readOnly = false)
	public int saves(UserBehaviorFilterVO userBehaviorFilterVO) {
		List<UserBehaviorFilter> list = new ArrayList<>();
		String userIds = userBehaviorFilterVO.getUserIds();
		String usernames = userBehaviorFilterVO.getUsernames();
		if(StringUtils.isNotBlank(userIds)) {
			String[] ids = userIds.split(",");
			String[] names = usernames.split(",");
			UserBehaviorFilter userBehaviorFilter = null;
			for (int i = 0; i < ids.length; i++) {
				if(ids[i].length() > 0) {
					userBehaviorFilter = new UserBehaviorFilter();
					userBehaviorFilter.setUserId(Long.valueOf(ids[i]));
					userBehaviorFilter.setUsername(names[i]);
					
					Date date = new Date();
					userBehaviorFilter.setId(IdGen.snowFlakeId());
					userBehaviorFilter.setCreateTime(date);
					userBehaviorFilter.setCreateBy(ShiroUtils.getPrincipal() != null ? ShiroUtils.getPrincipal().getId() : null);
					userBehaviorFilter.setDelFlag("0");
					userBehaviorFilter.setModifiedTime(date);
					userBehaviorFilter.setModifiedBy(ShiroUtils.getPrincipal() != null ? ShiroUtils.getPrincipal().getId() : null);
					
					list.add(userBehaviorFilter);
				}
			}
			
		}
		
		if(list.size() == 0) {
			return 1;
		}
		
		dao.removeAll();
		
		return dao.batchSave(list);
	}
}
