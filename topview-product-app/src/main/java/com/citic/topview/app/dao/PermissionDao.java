package com.citic.topview.app.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.app.entity.Permission;
import com.citic.topview.common.app.vo.PermissionVO;

@MyBatisDao
public interface PermissionDao extends CrudDao<Permission>{

	List<PermissionVO> list(Map<String, Object> params);
	
	List<PermissionVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);
}
