package com.citic.topview.app.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.bean.Query;
import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.app.entity.UpdateManager;
import com.citic.topview.common.app.vo.UpdateManagerVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.app.service.UpdateManagerService;

@Controller
@RequestMapping("/a/updateManager")
public class UpdateManagerController {

	@Autowired
	private UpdateManagerService updateManagerService;
	
	@ModelAttribute("updateManager")
	public UpdateManager get(@RequestParam(required=false) Long id) {
		UpdateManager entity = null;
		if (id != null && id > 0L){ 
			entity = updateManagerService.get(id);
		}
		if (entity == null){
			entity = new UpdateManager();
		}
		return entity;
	}
	
	@RequiresPermissions("updateManager:list")
	@RequestMapping("")
	public String updateManager(Model model) {
		return "app/updateManager/list";
	}
	
	@RequiresPermissions("updateManager:list")
	@Log("app应用列表")
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<UpdateManagerVO> updateManagers = updateManagerService.listQuery(query);
		int total = updateManagerService.count(query);
		
		PageUtils pageUtil = new PageUtils(updateManagers, total);
		return pageUtil;
	}
	
	@RequiresPermissions("updateManager:edit")
	@Log("app应用添加")
	@GetMapping("/add")
	public String add(Model model) {
		return "app/updateManager/add";
	}
	
	@RequiresPermissions("updateManager:edit")
	@Log("app应用编辑")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		UpdateManager updateManager = updateManagerService.get(id);
		model.addAttribute("entity", updateManager);
		return "app/updateManager/edit";
	}
	
	@RequiresPermissions("updateManager:edit")
	@Log("app应用保存")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("updateManager") UpdateManager updateManager) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(updateManagerService.save(updateManager) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("updateManager:edit")
	@Log("app应用更新")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("updateManager") UpdateManager updateManager) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(updateManagerService.update(updateManager) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("updateManager:edit")
	@Log("app应用删除")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(updateManagerService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}
