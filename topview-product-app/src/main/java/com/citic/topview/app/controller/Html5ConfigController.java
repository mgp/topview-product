package com.citic.topview.app.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.bean.Query;
import com.citic.topview.common.app.entity.Html5Config;
import com.citic.topview.common.app.vo.Html5ConfigVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.app.service.Html5ConfigService;

@Controller
@RequestMapping("/a/html5Config")
public class Html5ConfigController {

	@Autowired
	private Html5ConfigService html5ConfigService;
	
	@ModelAttribute("html5Config")
	public Html5Config get(@RequestParam(required=false) Long id) {
		Html5Config entity = null;
		if (id != null && id > 0L){
			entity = html5ConfigService.get(id);
		}
		if (entity == null){
			entity = new Html5Config();
		}
		return entity;
	}
	
	@RequiresPermissions("html5Config:list")
	@RequestMapping("")
	public String html5Config(Model model) {
		return "app/html5Config/list";
	}
	
	/*
	@RequestMapping("/list")
	@ResponseBody
	public List<Html5ConfigVO> list(@RequestParam Map<String, Object> params) {
		List<Html5ConfigVO> html5Configs = html5ConfigService.listQuery(params);
		return html5Configs;
	}
	*/
	
	@RequiresPermissions("html5Config:list")
	@Log("app第三方html5配置列表")
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<Html5ConfigVO> html5Configs = html5ConfigService.listQuery(query);
		int total = html5ConfigService.count(query);
		
		PageUtils pageUtil = new PageUtils(html5Configs, total);
		return pageUtil;
	}
	
	@RequiresPermissions("html5Config:edit")
	@Log("app第三方html5配置添加")
	@GetMapping("/add")
	public String add(Model model) {
		return "app/html5Config/add";
	}
	
	@RequiresPermissions("html5Config:edit")
	@Log("app第三方html5配置编辑")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		Html5Config html5Config = html5ConfigService.get(id);
		model.addAttribute("entity", html5Config);
		return "app/html5Config/edit";
	}
	
	@RequiresPermissions("html5Config:edit")
	@Log("app第三方html5配置保存")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("html5Config") Html5Config html5Config) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(html5ConfigService.save(html5Config) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("html5Config:edit")
	@Log("app第三方html5配置更新")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("html5Config") Html5Config html5Config) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(html5ConfigService.update(html5Config) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("html5Config:edit")
	@Log("app第三方html5配置删除")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(html5ConfigService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}