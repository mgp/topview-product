package com.citic.topview.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.app.entity.Html5Config;
import com.citic.topview.common.app.vo.Html5ConfigVO;
import com.citic.topview.app.dao.Html5ConfigDao;
import com.citic.topview.system.persistence.CrudService;

@Service
@Transactional(readOnly = true)
public class Html5ConfigService extends CrudService<Html5ConfigDao, Html5Config>{

	public Html5Config get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Html5Config html5Config) {
		return super.save(html5Config);
	}

	@Transactional(readOnly = false)
	public int update(Html5Config html5Config) {
		return super.update(html5Config);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<Html5ConfigVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<Html5ConfigVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}
}
