package com.citic.topview.app.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.app.service.UserBehaviorFilterService;
import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.app.entity.UserBehaviorFilter;
import com.citic.topview.common.app.vo.UserBehaviorFilterVO;
import com.citic.topview.common.bean.Query;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;

@Controller
@RequestMapping("/a/userBehaviorFilter")
public class UserBehaviorFilterController {

	@Autowired
	private UserBehaviorFilterService userBehaviorFilterService;
	
	@ModelAttribute("userBehaviorFilter")
	public UserBehaviorFilter get(@RequestParam(required=false) Long id) {
		UserBehaviorFilter entity = null;
		if (id != null && id > 0L){
			entity = userBehaviorFilterService.get(id);
		}
		if (entity == null){
			entity = new UserBehaviorFilter();
		}
		return entity;
	}
	
	@RequestMapping("")
	public String userBehaviorFilter(Model model) {
		return "app/userBehaviorFilter/list";
	}
	
	/*
	@RequestMapping("/list")
	@ResponseBody
	public List<UserBehaviorFilterVO> list(@RequestParam Map<String, Object> params) {
		List<UserBehaviorFilterVO> userBehaviorFilters = userBehaviorFilterService.listQuery(params);
		return userBehaviorFilters;
	}
	*/
	
	@Log("用户行为过滤列表")
	@RequiresPermissions("userBehaviorFilter:list")
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<UserBehaviorFilterVO> userBehaviorFilters = userBehaviorFilterService.listQuery(query);
		PageUtils pageUtil = new PageUtils(userBehaviorFilters, 0);
		return pageUtil;
	}
	
	@Log("用户行为过滤保存")
	@RequiresPermissions("userBehaviorFilter:edit")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(UserBehaviorFilterVO userBehaviorFilterVO) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(userBehaviorFilterService.saves(userBehaviorFilterVO) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("用户行为过滤删除")
	@RequiresPermissions("userBehaviorFilter:edit")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(userBehaviorFilterService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}
