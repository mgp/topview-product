package com.citic.topview.app.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.app.entity.UserBehavior;
import com.citic.topview.common.app.vo.UserBehaviorVO;

@MyBatisDao
public interface UserBehaviorDao extends CrudDao<UserBehavior>{

	List<UserBehaviorVO> list(Map<String, Object> params);
	
	List<UserBehaviorVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);
}
