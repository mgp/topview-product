package com.citic.topview.app.mobile.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.vo.AppResultVO;
import com.citic.topview.common.vo.ResultVO;

@Controller
@RequestMapping("/app/test")
public class AppTestController {

	@ResponseBody
	@RequestMapping(value = {"list"})
	public AppResultVO<Object> checkPadLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		return setResult(ResultVO.SUCCESS, "login success", null);
	}
	
	public AppResultVO<Object> setResult(String result, String message, String remark) {
		AppResultVO<Object> mobileResultVO = new AppResultVO<Object>();
		mobileResultVO.setResult(result);
		mobileResultVO.setMessage(message);
		mobileResultVO.setRemark(remark);
		return mobileResultVO;
	}
}
