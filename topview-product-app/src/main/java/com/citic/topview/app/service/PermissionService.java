package com.citic.topview.app.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.app.dao.PermissionDao;
import com.citic.topview.common.app.entity.Permission;
import com.citic.topview.common.app.vo.PermissionVO;
import com.citic.topview.common.persistence.IPermissionEntity;
import com.citic.topview.common.system.entity.User;
import com.citic.topview.system.persistence.CrudService;
import com.citic.topview.system.service.OfficeService;
import com.citic.topview.system.service.TagUserService;
import com.citic.topview.system.util.ShiroUtils;

@Service
@Transactional(readOnly = true)
public class PermissionService extends CrudService<PermissionDao, Permission>{

	@Autowired
	TagUserService tagUserService;
	
	@Autowired
	OfficeService officeService;
	
	public Permission get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Permission permission) {
		return super.save(permission);
	}

	@Transactional(readOnly = false)
	public int update(Permission permission) {
		return super.update(permission);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<PermissionVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<PermissionVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}

	public boolean hasPermission(IPermissionEntity permission) {
		String permissionOrg = permission.getPermissionOrg();
		String permissionTag = permission.getPermissionTag();
		String permissionUser = permission.getPermissionUser();
		String permissionStatus = permission.getPermissionStatus();
		
		if (permissionStatus.equals("0")) {
			return false;
		}
		if (permissionStatus.equals("1")) {
			return true;
		}
		
		//当permissionStatus=指定范围时 
		if (StringUtils.isBlank(permissionOrg) && StringUtils.isBlank(permissionTag) && StringUtils.isBlank(permissionUser)) { 
			return false;
		}
		User u = ShiroUtils.getUser();
		if (StringUtils.isNotBlank(permissionUser)) {
			String idx = u.getId() + ",";
			if (permissionUser.indexOf(idx) != -1) {
				return true;
			}
		}
		if (StringUtils.isNotBlank(permissionOrg)) {
			Set<String> orgIds = officeService.getOfficeIdsByUser(u);
			for (String id : orgIds) {
				String idx = id + ",";
				if (permissionOrg.indexOf(idx) != -1) {
					return true;
				}
			}
		}
		//标签
		if (StringUtils.isNotBlank(permissionTag)) {
			Set<String> tagIds = tagUserService.getTagIdsByUser(u);
			for (String id : tagIds) {
				String idx = id + ",";
				if (permissionTag.indexOf(idx) != -1) {
					return true;
				}
			}
		}
		return false;
	}
}
