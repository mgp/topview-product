package com.citic.topview.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.app.entity.UserBehavior;
import com.citic.topview.common.app.vo.UserBehaviorVO;
import com.citic.topview.system.persistence.CrudService;
import com.citic.topview.app.dao.UserBehaviorDao;

@Service
@Transactional(readOnly = true)
public class UserBehaviorService extends CrudService<UserBehaviorDao, UserBehavior>{

	public UserBehavior get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(UserBehavior userBehavior) {
		return super.save(userBehavior);
	}

	@Transactional(readOnly = false)
	public int update(UserBehavior userBehavior) {
		return super.update(userBehavior);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<UserBehaviorVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<UserBehaviorVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}
}
