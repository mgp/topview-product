package com.citic.topview.app.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.app.entity.Html5Config;
import com.citic.topview.common.app.vo.Html5ConfigVO;

@MyBatisDao
public interface Html5ConfigDao extends CrudDao<Html5Config>{

	List<Html5ConfigVO> list(Map<String, Object> params);
	
	List<Html5ConfigVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);
}
