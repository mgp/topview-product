package com.citic.topview.app.mobile.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.app.service.ExtHostService;
import com.citic.topview.common.app.vo.ExtHostVO;
import com.citic.topview.common.vo.AppResultVO;
import com.citic.topview.system.controller.BaseController;

@Controller
@RequestMapping("/app/extHost")
public class AppExtHostController extends BaseController{

	@Autowired
	private ExtHostService extHostService;
	
	@RequestMapping("list")
	@ResponseBody
	public AppResultVO<Object> list() {
		AppResultVO<Object> result = new AppResultVO<Object>();
		try {
			List<ExtHostVO> list = extHostService.list(new HashMap<>());
			Map<String, String> map = new HashMap<String, String>();
			for (ExtHostVO h : list) {
				map.put(h.getHostKey(), h.getHostValue());
			}
			result.setResult(AppResultVO.SUCCESS);
			result.setBizVO(map);
		} catch (Exception e) {
			logger.error("获取外部系统失败", e);
			result.setResult(AppResultVO.ERROR);
			result.setMessage("获取外部系统失败：" + e.getMessage());
		}
		return result;
	}
}
