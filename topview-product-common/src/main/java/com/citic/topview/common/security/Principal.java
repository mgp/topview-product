package com.citic.topview.common.security;

import java.io.Serializable;

import com.citic.topview.common.system.entity.User;

public class Principal implements Serializable {
	
	private static final long serialVersionUID = -2782665063989073556L;
	
	private Long id; // 编号
	private String loginName; // 登录名
	private String name; // 姓名
	private boolean mobileLogin; // 是否手机登录
	
	public Principal() {
		
	}
	
	public Principal(Long id, String loginName, String name, boolean mobileLogin){
		this.id = id;
		this.loginName = loginName;
		this.name = name;
		this.mobileLogin = mobileLogin;
	}
	
	public Principal(User user, boolean mobileLogin) {
		this.id = user.getId();
		this.loginName = user.getLoginName();
		this.name = user.getName();
		this.mobileLogin = mobileLogin;
	}

	public Long getId() {
		return id;
	}

	public String getLoginName() {
		return loginName;
	}

	public String getName() {
		return name;
	}

	public boolean isMobileLogin() {
		return mobileLogin;
	}

	@Override
	public String toString() {
		return String.valueOf(id);
	}
}
