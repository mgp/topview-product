package com.citic.topview.common.system.entity;

import com.citic.topview.common.persistence.DataEntity;

public class Role extends DataEntity<Role>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	private String name; 		// 角色名称
	private String enname;		// 英文名称
	private String roleType;	// 权限类型
	private String dataScope;	// 数据范围
	private String isSys; 	//是否是系统数据
	private String useable; 	//是否是可用
	
	public Role() {
		
	}
	
	public Role(long id, String name, String enname) {
		this.id = id;
		this.name = name;
		this.enname = enname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEnname() {
		return enname;
	}

	public void setEnname(String enname) {
		this.enname = enname;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getDataScope() {
		return dataScope;
	}

	public void setDataScope(String dataScope) {
		this.dataScope = dataScope;
	}

	public String getIsSys() {
		return isSys;
	}

	public void setIsSys(String isSys) {
		this.isSys = isSys;
	}

	public String getUseable() {
		return useable;
	}

	public void setUseable(String useable) {
		this.useable = useable;
	}

	@Override
	public String toString() {
		return "Role{" +
				"id=" + id +
				", name='" + name + '\'' +
				", enname='" + enname + '\'' +
				", roleType='" + roleType + '\'' +
				", dataScope=" + dataScope +
				", isSys='" + isSys + '\'' +
				", useable=" + useable +
				'}';
	}
	
    public static class RoleBuilder {
        private Role role = new Role();

        public RoleBuilder withId(Long id) {
        	role.setId(id);
            return this;
        }

        public RoleBuilder withName(String name) {
        	role.setName(name);
            return this;
        }
        
        public RoleBuilder withEnname(String enname) {
        	role.setEnname(enname);
        	return this;
        }

        public Role build() {
            return role;
        }
    }
}
