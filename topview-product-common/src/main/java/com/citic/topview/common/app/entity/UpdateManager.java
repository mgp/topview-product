package com.citic.topview.common.app.entity;

import com.citic.topview.common.persistence.DataEntity;

public class UpdateManager extends DataEntity<UpdateManager>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	private String name;					// 应用名称  
	private String versionName;					// 版本名称  
	private String versionCode;					// 版本号  
	private String plistUrl;					// plist地址  
	private String downloadUrl;					// 下载地址  
	private String size;					// 应用包大小  
	private String type;					// 0：正常 1：不正常  
	private String status;					// 状态 0：test 1：dev 2：prod  
	private String forceFlag;					// 是否强制更新 0：强制 1：不强制  
	private String description;					// 描述  

	public UpdateManager() {
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}	
	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}	
	public String getPlistUrl() {
		return plistUrl;
	}

	public void setPlistUrl(String plistUrl) {
		this.plistUrl = plistUrl;
	}	
	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}	
	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}	
	public String getForceFlag() {
		return forceFlag;
	}

	public void setForceFlag(String forceFlag) {
		this.forceFlag = forceFlag;
	}	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	

	@Override
	public String toString() {
		return "UpdateManager{" +
				"id=" + id +
				", name='" + name + '\'' +
				", versionName='" + versionName + '\'' +
				", versionCode='" + versionCode + '\'' +
				", plistUrl='" + plistUrl + '\'' +
				", downloadUrl='" + downloadUrl + '\'' +
				", size='" + size + '\'' +
				", type='" + type + '\'' +
				", status='" + status + '\'' +
				", forceFlag='" + forceFlag + '\'' +
				", description='" + description + '\'' +
				'}';
	}
}
