package com.citic.topview.common.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AjaxResultVO<T> extends ResultVO {
	
	private static final long serialVersionUID = 9139270051915023753L;
	
	public static final String ERROR = "error";
	public static final String SUCCESS = "success";

	private T bizVO; 			// ajax请求结果VO
	
	public AjaxResultVO() {
		super();
		this.setResult(SUCCESS);
	}

	public T getBizVO() {
		return bizVO;
	}

	public void setBizVO(T bizVO) {
		this.bizVO = bizVO;
	}
	
	@JsonIgnore
	public boolean isSuccess() {
		return SUCCESS.equals(this.getResult());
	}

	@JsonIgnore
	public boolean isError() {
		return ERROR.equals(this.getResult());
	}
}
