package com.citic.topview.common.system.vo;

import com.citic.topview.common.persistence.DataEntity;

public class GenVO extends DataEntity<GenVO>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	private String entityName;					// 实体名  
	private String tableName;					// 表名  
	private String packageName;					// 包名前缀  
	private String module;					// 标签名称  
	private String permission;					// 标签名称  
	private String filePrefix;					// 文件保存前缀路径  

	public GenVO() {
		
	}
	
	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}	
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}	
	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}	
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}	
	public String getFilePrefix() {
		return filePrefix;
	}

	public void setFilePrefix(String filePrefix) {
		this.filePrefix = filePrefix;
	}	

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	@Override
	public String toString() {
		return "Gen{" +
				"id=" + id +
				", entityName='" + entityName + '\'' +
				", tableName='" + tableName + '\'' +
				", packageName='" + packageName + '\'' +
				", permission='" + permission + '\'' +
				", module='" + module + '\'' +
				", filePrefix='" + filePrefix + '\'' +
				'}';
	}
}
