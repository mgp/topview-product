package com.citic.topview.common.system.vo;

import java.util.Date;

import com.citic.topview.common.json.LongJsonDeserializer;
import com.citic.topview.common.json.LongJsonSerializer;
import com.citic.topview.common.persistence.DataEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserVO extends DataEntity<UserVO>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long companyId;					// 归属公司  
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long officeId;					// 归属部门  
	private String loginName;					// 登录名  
	private String password;					// 密码  
	private String newPassword;					// 密码  
	private String confirmPassword;					// 密码  
	private String no;					// 工号  
	private String code;					// code  
	private String name;					// 姓名  
	private String gender;					// 性别  
	private String status;					// 状态  
	private String email;					// 邮箱  
	private String mobile;					// 手机  
	private String userType;					// 用户类型  
	private String loginIp;					// 最后登陆IP  
	private Date loginDate;					// 最后登陆日期  
	private String photo;					// 头像  

	public UserVO() {
		
	}
	
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}	
	public Long getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Long officeId) {
		this.officeId = officeId;
	}	
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}	
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}	
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}	
	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}	
	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}	
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}	

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", companyId='" + companyId + '\'' +
				", officeId='" + officeId + '\'' +
				", loginName='" + loginName + '\'' +
				", password='" + password + '\'' +
				", no='" + no + '\'' +
				", code='" + code + '\'' +
				", name='" + name + '\'' +
				", gender='" + gender + '\'' +
				", status='" + status + '\'' +
				", email='" + email + '\'' +
				", mobile='" + mobile + '\'' +
				", userType='" + userType + '\'' +
				", loginIp='" + loginIp + '\'' +
				", loginDate='" + loginDate + '\'' +
				", photo='" + photo + '\'' +
				'}';
	}
}
