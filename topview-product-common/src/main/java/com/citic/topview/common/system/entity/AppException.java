package com.citic.topview.common.system.entity;

import java.util.Date;

import com.citic.topview.common.json.LongJsonDeserializer;
import com.citic.topview.common.json.LongJsonSerializer;
import com.citic.topview.common.persistence.DataEntity;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class AppException extends DataEntity<AppException>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long userId;					// 异常代码  
	private String code;					// 异常代码  
	private String message;					// 异常信息  
	private String exception;					// 异常堆栈  
	private String version;					// 移动应用版本  
	private String phoneInfo;					// 移动设备信息  
	private Date createExceptionTime;					// 移动端发生异常时间  

	public AppException() {
		
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	
	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}	
	public String getPhoneInfo() {
		return phoneInfo;
	}

	public void setPhoneInfo(String phoneInfo) {
		this.phoneInfo = phoneInfo;
	}	
	public Date getCreateExceptionTime() {
		return createExceptionTime;
	}

	public void setCreateExceptionTime(Date createExceptionTime) {
		this.createExceptionTime = createExceptionTime;
	}	

	@Override
	public String toString() {
		return "AppException{" +
				"id=" + id +
				", code='" + code + '\'' +
				", message='" + message + '\'' +
				", exception='" + exception + '\'' +
				", version='" + version + '\'' +
				", phoneInfo='" + phoneInfo + '\'' +
				", createExceptionTime='" + createExceptionTime + '\'' +
				'}';
	}
}
