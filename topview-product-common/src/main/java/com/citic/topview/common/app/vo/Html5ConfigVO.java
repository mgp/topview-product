package com.citic.topview.common.app.vo;

import com.citic.topview.common.persistence.DataEntity;

public class Html5ConfigVO extends DataEntity<Html5ConfigVO>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	private String name;					// 名称  
	private String keyFun;					// 标识（跟app_permssion表对应）  
	private String url;					// html5链接  
	private String method;					// 链接方法（post，get）  
	private String scrollEnabled;					// 启用滑动  
	private String scalesPageFit;					// 启用缩放功能  

	public Html5ConfigVO() {
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	public String getKeyFun() {
		return keyFun;
	}

	public void setKeyFun(String keyFun) {
		this.keyFun = keyFun;
	}	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}	
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}	
	public String getScrollEnabled() {
		return scrollEnabled;
	}

	public void setScrollEnabled(String scrollEnabled) {
		this.scrollEnabled = scrollEnabled;
	}	
	public String getScalesPageFit() {
		return scalesPageFit;
	}

	public void setScalesPageFit(String scalesPageFit) {
		this.scalesPageFit = scalesPageFit;
	}	

	@Override
	public String toString() {
		return "Html5Config{" +
				"id=" + id +
				", name='" + name + '\'' +
				", keyFun='" + keyFun + '\'' +
				", url='" + url + '\'' +
				", method='" + method + '\'' +
				", scrollEnabled='" + scrollEnabled + '\'' +
				", scalesPageFit='" + scalesPageFit + '\'' +
				'}';
	}
}
