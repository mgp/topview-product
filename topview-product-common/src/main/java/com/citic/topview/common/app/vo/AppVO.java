package com.citic.topview.common.app.vo;

import java.io.Serializable;

public class AppVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String uniqueId; 		// 设备编号

	private String username; 		// 手机号码

	private String password; 		// 用户密码

	private String verifyCode;		// 验证码

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}
}
