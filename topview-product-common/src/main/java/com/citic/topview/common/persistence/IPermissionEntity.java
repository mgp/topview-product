package com.citic.topview.common.persistence;



public interface IPermissionEntity {

	/**
	 * 权限-机构
	 * @return
	 */
	public String getPermissionOrg();
	public void setPermissionOrg(String org);
	
	/**
	 * 权限-标签
	 * @return
	 */
	public String getPermissionTag();
	public void setPermissionTag(String tag);
	
	/**
	 * 权限-用户
	 * @return
	 */
	public String getPermissionUser();
	public void setPermissionUser(String user);
	
	/**
	 * 权限-状态
	 * @return
	 */
	public String getPermissionStatus();
	public void setPermissionStatus(String status);
	
	/**
	 * org、tag、user权限初始化json数据
	 * @return
	 */
	public String getInputPermissionJson();
	public void setInputPermissionJson(String inputPermissionJson);
}
