package com.citic.topview.common.util;

public class StringUtil {

    public static String charToLowerCase(String str){  
        char[] ch = str.toCharArray();  
        StringBuffer sbf = new StringBuffer();  
        for(int i=0; i< ch.length; i++){  
            sbf.append(charToLowerCase(ch[i]));  
        }  
        return sbf.toString();  
    }
    
    public static String firstCharToLowerCase(String string){  
		String str = string.substring(0, 1);
		string = string.substring(1, string.length());
    	return charToLowerCase(str) + string;  
    }
    
    public static char charToLowerCase(char ch){  
        if(ch <= 90 && ch >= 65){  
            ch += 32;  
        }  
        return ch;  
    } 
    
	//首字母转小写
    public static String toLowerCaseFirstOne(String s){
      if(Character.isLowerCase(s.charAt(0)))
        return s;
      else
        return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
    }
    
	//首字母转大写
    public static String toUpperCaseFirstOne(String s){
      if(Character.isUpperCase(s.charAt(0)))
        return s;
      else
        return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
    }
}
