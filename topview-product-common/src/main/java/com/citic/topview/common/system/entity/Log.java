package com.citic.topview.common.system.entity;

import com.citic.topview.common.json.LongJsonDeserializer;
import com.citic.topview.common.json.LongJsonSerializer;
import com.citic.topview.common.persistence.DataEntity;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Log extends DataEntity<Log>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long userId;					// 用户ID  
	private String username;					// 用户名  
	private String operation;					// 用户操作  
	private Long time;					// 响应时间  
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private String url;					// 请求方法  
	private String method;					// 请求方法  
	private String params;					// 请求参数  
	private String ip;					// IP地址  

	public Log() {
		
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}	
	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}	
	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}	
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}	
	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}	

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Log{" +
				"id=" + id +
				", userId='" + userId + '\'' +
				", username='" + username + '\'' +
				", operation='" + operation + '\'' +
				", time='" + time + '\'' +
				", method='" + method + '\'' +
				", params='" + params + '\'' +
				", ip='" + ip + '\'' +
				'}';
	}
}
