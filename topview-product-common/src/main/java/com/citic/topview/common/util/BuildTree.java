package com.citic.topview.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.citic.topview.common.bean.Tree;
import com.citic.topview.common.system.vo.MenuVO;
import com.citic.topview.common.system.vo.OfficeVO;
import com.citic.topview.common.system.vo.TagVO;

/**
 * 数据格式
[
	{
        "attributes": {"id": "0"},
        "parent": "0",
        "state": {"opened": false},
        "text": "test1",
        "children": [
            {
                "attributes": {"id": "1"},
                "parent": "0",
                "state": {"opened": true},
                "text": "test1.1.txt",
                "type": "leaf"
            },
            {
                "attributes": {"id": "2"},
                "parent": "0",
                "state": {"opened": true},
                "text": "test1.2.txt",
                "type": "leaf"
            }
        ]
    },
    {},
    {},
    {},
    {},
    ...
]
 * @author Administrator
 *
 */
public class BuildTree {

	public static Tree<OfficeVO> buildOffice(List<Tree<OfficeVO>> trees) {
		if (trees == null) {
			return null;
		}
		
		List<Tree<OfficeVO>> list = new ArrayList<Tree<OfficeVO>>();
		
		for (Tree<OfficeVO> children : trees) {
			Long pid = children.getParentId();
			if (pid == null || pid == 0L) {
				list.add(children); // 放入顶层节点
				continue;
			}
			
			for (Tree<OfficeVO> parent : trees) {
				Long id = parent.getId();
				if (id != null && id.equals(pid)) {
					parent.getChildren().add(children);
					children.setHasParent(true);
					parent.setChildren(true);
					continue;
				}
			}
		}
		
		Tree<OfficeVO> root = new Tree<OfficeVO>();
		if (list.size() == 1) {
			root = list.get(0);
		} else {
			root.setId(-1L);
			root.setParentId(null);
			root.setHasParent(false);
			root.setChildren(true);
			root.setChecked(true);
			root.setChildren(list);
			root.setText("顶级节点");
			Map<String, Object> state = new HashMap<String, Object>(16);
			state.put("opened", true);
			root.setState(state);
		}

		return root;
	}

	public static Tree<MenuVO> buildMenu(List<Tree<MenuVO>> trees) {
		if (trees == null) {
			return null;
		}
		
		List<Tree<MenuVO>> list = new ArrayList<Tree<MenuVO>>();
		
		for (Tree<MenuVO> children : trees) {
			Long pid = children.getParentId();
			if (pid == null || pid == 0L) {
				list.add(children); // 放入顶层节点
				continue;
			}
			
			for (Tree<MenuVO> parent : trees) {
				Long id = parent.getId();
				if (id != null && id.equals(pid)) {
					parent.getChildren().add(children);
					children.setHasParent(true);
					parent.setChildren(true);
					continue;
				}
			}
		}
		
		Tree<MenuVO> root = new Tree<MenuVO>();
		if (list.size() == 1) {
			root = list.get(0);
		} else {
			root.setId(-1L);
			root.setParentId(null);
			root.setHasParent(false);
			root.setChildren(true);
			root.setChecked(true);
			root.setChildren(list);
			root.setText("顶级节点");
			Map<String, Object> state = new HashMap<String, Object>(16);
			state.put("opened", true);
			root.setState(state);
		}

		return root;
	}

	public static Tree<TagVO> buildTag(List<Tree<TagVO>> trees) {
		if (trees == null) {
			return null;
		}
		
		List<Tree<TagVO>> list = new ArrayList<Tree<TagVO>>();
		
		for (Tree<TagVO> children : trees) {
			Long pid = children.getParentId();
			if (pid == null || pid == 0L) {
				list.add(children); // 放入顶层节点
				continue;
			}
			
			for (Tree<TagVO> parent : trees) {
				Long id = parent.getId();
				if (id != null && id.equals(pid)) {
					parent.getChildren().add(children);
					children.setHasParent(true);
					parent.setChildren(true);
					continue;
				}
			}
		}
		
		Tree<TagVO> root = new Tree<TagVO>();
		if (list.size() == 1) {
			root = list.get(0);
		} else {
			root.setId(-1L);
			root.setParentId(null);
			root.setHasParent(false);
			root.setChildren(true);
			root.setChecked(true);
			root.setChildren(list);
			root.setText("顶级节点");
			Map<String, Object> state = new HashMap<String, Object>(16);
			state.put("opened", true);
			root.setState(state);
		}

		return root;
	}
}
