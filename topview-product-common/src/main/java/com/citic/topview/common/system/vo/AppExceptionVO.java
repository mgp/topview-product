package com.citic.topview.common.system.vo;

import java.io.Serializable;
import java.util.Date;

import com.citic.topview.common.json.LongJsonDeserializer;
import com.citic.topview.common.json.LongJsonSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class AppExceptionVO implements Serializable{

	private static final long serialVersionUID = 8047768429373875446L;
	
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long id;
	@JsonSerialize(using = LongJsonSerializer.class)
	@JsonDeserialize(using = LongJsonDeserializer.class)
	private Long userId;
	private String code;
	private String message;
	private String exception;
	private String version;
	private String phoneInfo;
	private String createTime;
	private Date createExceptionTime;
	
	public AppExceptionVO() {
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}


	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPhoneInfo() {
		return phoneInfo;
	}

	public void setPhoneInfo(String phoneInfo) {
		this.phoneInfo = phoneInfo;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Date getCreateExceptionTime() {
		return createExceptionTime;
	}

	public void setCreateExceptionTime(Date createExceptionTime) {
		this.createExceptionTime = createExceptionTime;
	}

	@Override
	public String toString() {
		return "AppException{" +
				", code='" + code + '\'' +
				", message='" + message + '\'' +
				", exception='" + exception + '\'' +
				", version='" + version + '\'' +
				", phoneInfo='" + phoneInfo + '\'' +
				", createTime='" + createTime + '\'' +
				'}';
	}
}
