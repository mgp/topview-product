package com.citic.topview.common.vo;

import java.io.Serializable;

public class ResultVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public final static String ERROR="error";
	public final static String SUCCESS="success";
	
	private String result;
	private String message;
	private String errorType;
	private String description;
	private Object remark;
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static String ERROR_TYPE_SYSTEM = "system";

	public static String ERROR_TYPE_BUSINESS = "business";

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public Object getRemark() {
		return remark;
	}

	public void setRemark(Object remark) {
		this.remark = remark;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
