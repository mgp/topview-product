package com.citic.topview.common.system.entity;

import com.citic.topview.common.persistence.DataEntity;

public class Dict extends DataEntity<Dict>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	private String value;					// 键值  
	private String label;					// 标签名  
	private String type;					// 类型  
	private String description;					// 描述  
	private Integer sort;					// 排序  

	public Dict() {
		
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}	

	@Override
	public String toString() {
		return "Dict{" +
				"id=" + id +
				", value='" + value + '\'' +
				", label='" + label + '\'' +
				", type='" + type + '\'' +
				", description='" + description + '\'' +
				", sort='" + sort + '\'' +
				'}';
	}
}
