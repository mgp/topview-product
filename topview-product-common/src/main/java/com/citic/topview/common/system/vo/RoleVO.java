package com.citic.topview.common.system.vo;

import com.citic.topview.common.persistence.DataEntity;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoleVO extends DataEntity<RoleVO>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	private String name; 		// 角色名称
	private String enname;		// 英文名称
	private String roleType;	// 权限类型
	private Byte dataScope;	// 数据范围
	
	private String isSys; 	//是否是系统数据
	private String useable; 	//是否是可用
	
	private Boolean show; 	//是否是可用
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEnname() {
		return enname;
	}

	public void setEnname(String enname) {
		this.enname = enname;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public Byte getDataScope() {
		return dataScope;
	}

	public void setDataScope(Byte dataScope) {
		this.dataScope = dataScope;
	}

	public String getIsSys() {
		return isSys;
	}

	public void setIsSys(String isSys) {
		this.isSys = isSys;
	}

	public String getUseable() {
		return useable;
	}

	public void setUseable(String useable) {
		this.useable = useable;
	}

	public Boolean getShow() {
		return show;
	}

	public void setShow(Boolean show) {
		this.show = show;
	}

	@Override
	public String toString() {
		return "RoleVO{" +
				"id=" + id +
				", name='" + name + '\'' +
				", enname='" + enname + '\'' +
				", roleType='" + roleType + '\'' +
				", dataScope=" + dataScope +
				", isSys='" + isSys + '\'' +
				", useable=" + useable +
				'}';
	}
}
