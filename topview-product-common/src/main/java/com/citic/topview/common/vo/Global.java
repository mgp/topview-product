package com.citic.topview.common.vo;

public class Global {

	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String YES = "1";
	public static final String NO = "0";
}
