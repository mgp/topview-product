package com.citic.topview.common.system.entity;

import java.io.Serializable;

public class RoleMenu implements Serializable{

	private static final long serialVersionUID = -7028535755275990169L;

	private Long id;
    
    private Long roleId;

    private Long menuId;

    public RoleMenu() {
    	
    }
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }
}