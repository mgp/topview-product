package com.citic.topview.common.persistence;

public interface CrudDao<T> {

	public T get(Long id);
	
	public int save(T entity);
	
	public int update(T entity);
	
	public int deleteByDelFlag(Long id);
	
	public int remove(Long id);
	
	public int batchRemove(Long[] ids);
}
