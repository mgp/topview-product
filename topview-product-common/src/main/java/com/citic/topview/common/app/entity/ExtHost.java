package com.citic.topview.common.app.entity;

import com.citic.topview.common.persistence.DataEntity;

public class ExtHost extends DataEntity<ExtHost>{

	private static final long serialVersionUID = 8047768429373875446L;
	
	private String name;					// 名称  
	private String hostKey;					// 域名KEY  
	private String hostValue;					// 域名  

	public ExtHost() {
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	public String getHostKey() {
		return hostKey;
	}

	public void setHostKey(String hostKey) {
		this.hostKey = hostKey;
	}	
	public String getHostValue() {
		return hostValue;
	}

	public void setHostValue(String hostValue) {
		this.hostValue = hostValue;
	}	

	@Override
	public String toString() {
		return "ExtHost{" +
				"id=" + id +
				", name='" + name + '\'' +
				", hostKey='" + hostKey + '\'' +
				", hostValue='" + hostValue + '\'' +
				'}';
	}
}
