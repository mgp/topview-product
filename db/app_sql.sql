SET FOREIGN_KEY_CHECKS=0;


-- ----------------------------
-- Table structure for `app_ext_host`
-- ----------------------------
DROP TABLE IF EXISTS `app_ext_host`;
CREATE TABLE `app_ext_host` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  `host_key` varchar(64) DEFAULT NULL COMMENT '域名KEY',
  `host_value` varchar(64) DEFAULT NULL COMMENT '域名',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='外系统域名';


-- ----------------------------
-- Table structure for `app_update_manager`
-- ----------------------------
DROP TABLE IF EXISTS `app_update_manager`;
CREATE TABLE `app_update_manager` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `name` varchar(64) DEFAULT NULL COMMENT '应用名称',
  `version_name` varchar(64) DEFAULT NULL COMMENT '版本名称',
  `version_code` varchar(64) DEFAULT NULL COMMENT '版本号',
  `plist_url` varchar(64) DEFAULT NULL COMMENT 'plist地址',
  `download_url` varchar(64) DEFAULT NULL COMMENT '下载地址',
  `size` varchar(16) DEFAULT NULL COMMENT '应用包大小',
  `type` char(1) DEFAULT NULL COMMENT '0：正常 1：不正常',
  `status` char(1) DEFAULT NULL COMMENT '状态 0：test 1：dev 2：prod',
  `force_flag` char(1) DEFAULT NULL COMMENT '是否强制更新 0：强制 1：不强制',
  `description` varchar(64) DEFAULT NULL COMMENT '描述',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='app更新管理';


-- ----------------------------
-- Table structure for `app_permission`
-- ----------------------------
DROP TABLE IF EXISTS `app_permission`;
CREATE TABLE `app_permission` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `parent_id` bigint(20) DEFAULT NULL COMMENT 'PID',
  `name` varchar(64) DEFAULT NULL COMMENT '菜单名',
  `key_fun` varchar(64) DEFAULT NULL COMMENT '菜单的唯一标识',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `type` char(1) DEFAULT NULL COMMENT '类型',
  `html5` char(1) DEFAULT NULL COMMENT '是否是html5',
  `sort` decimal(10) DEFAULT NULL COMMENT '排序',
  `permission_status` char(1) DEFAULT NULL COMMENT '权限状态0：全部不可见，1：全部可见，2部分可见',
  `permission_org` text DEFAULT NULL COMMENT '机构权限',
  `permission_tag` text DEFAULT NULL COMMENT '标签权限',
  `permission_user` text DEFAULT NULL COMMENT '用户权限',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='app菜单权限';


-- ----------------------------
-- Table structure for `app_avoid_login`
-- ----------------------------
DROP TABLE IF EXISTS `app_avoid_login`;
CREATE TABLE `app_avoid_login` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `unique_id` varchar(255) DEFAULT NULL COMMENT '设备ID',
  `is_avoid_login` char(1) DEFAULT NULL COMMENT '是否免登录',
  `is_finger_login` char(1) DEFAULT NULL COMMENT '是否开启指纹登录',
  `is_binding` char(1) DEFAULT NULL COMMENT '绑定：0表示未绑定，1表示绑定',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='app免登录';


-- ----------------------------
-- Table structure for `app_user_behavior`
-- ----------------------------
DROP TABLE IF EXISTS `app_user_behavior`;
CREATE TABLE `app_user_behavior` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(64) DEFAULT NULL COMMENT '用户名',
  `current_page` varchar(255) DEFAULT NULL COMMENT '当前页面',
  `action` varchar(255) DEFAULT NULL COMMENT '操作',
  `goto_page` varchar(255) DEFAULT NULL COMMENT '进入页面',
  `module` varchar(64) DEFAULT NULL COMMENT '模块',
  `trigger_time` datetime DEFAULT NULL COMMENT '触发时间',
  `longitude` varchar(255) DEFAULT NULL COMMENT '经度',
  `latitude` varchar(255) DEFAULT NULL COMMENT '纬度',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `version` varchar(45) DEFAULT NULL COMMENT '版本',
  `platform` varchar(45) DEFAULT NULL COMMENT '平台',
  `stay_time` int(11) DEFAULT NULL COMMENT '驻留时间',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='app用户行为';


-- ----------------------------
-- Table structure for `app_user_behavior_filter`
-- ----------------------------
DROP TABLE IF EXISTS `app_user_behavior_filter`;
CREATE TABLE `app_user_behavior_filter` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `username` varchar(64) DEFAULT NULL COMMENT '用户名',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='app用户行为过滤用户';


-- ----------------------------
-- Table structure for `app_html5_config`
-- ----------------------------
DROP TABLE IF EXISTS `app_html5_config`;
CREATE TABLE `app_html5_config` (
  `id` bigint(20) NOT NULL COMMENT 'ID',
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  `key_fun` varchar(64) DEFAULT NULL COMMENT '标识（跟app_permssion表对应）',  
  `url` varchar(255) DEFAULT NULL COMMENT 'html5链接',
  `method` varchar(16) DEFAULT NULL COMMENT '链接方法（post，get）',
  `scroll_enabled` char(1) DEFAULT NULL COMMENT '启用滑动',
  `scales_page_fit` char(1) DEFAULT NULL COMMENT '启用缩放功能',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='app第三方html5配置';










