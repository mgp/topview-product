SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL,
  `name` varchar(64) DEFAULT NULL COMMENT '角色名称',
  `enname` varchar(64) DEFAULT NULL COMMENT '英文名称',
  `role_type` varchar(64) DEFAULT NULL COMMENT '权限类型',
  `data_scope` char(4) DEFAULT NULL COMMENT '数据范围',
  `is_sys` varchar(64) DEFAULT NULL COMMENT '是否是系统数据',
  `useable` varchar(64) DEFAULT NULL COMMENT '是否是可用',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色';


-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `href` varchar(255) DEFAULT NULL COMMENT '菜单URL',
  `permission` varchar(255) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` char(1) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `sort` decimal(10) DEFAULT NULL COMMENT '排序',
  `is_show` char(1) DEFAULT NULL COMMENT '是否在菜单中显示（1：显示；0：不显示）',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单管理';


-- ----------------------------
-- Table structure for `sys_office`
-- ----------------------------
DROP TABLE IF EXISTS `sys_office`;
CREATE TABLE `sys_office` (
  `id` bigint(20) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `code` varchar(255) DEFAULT NULL COMMENT '构编码',
  `type` char(1) DEFAULT NULL COMMENT '机构类型（1：公司；2：部门；3：小组）',
  `grade` char(1) DEFAULT NULL COMMENT '机构等级（1：一级；2：二级；3：三级；4：四级）',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `phone` varchar(50) DEFAULT NULL COMMENT '电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮件',
  `useable` char(1) DEFAULT NULL COMMENT '是否可用',
  `sort` decimal(10) DEFAULT NULL COMMENT '排序',
  `primary_person` bigint(20) DEFAULT NULL COMMENT '主负责人',
  `deputy_person` bigint(20) DEFAULT NULL COMMENT '副负责人',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单管理';


-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL,
  `company_id` bigint(20) DEFAULT NULL COMMENT '归属公司',
  `office_id` bigint(20) DEFAULT NULL COMMENT '归属部门',
  `login_name` varchar(128) DEFAULT NULL COMMENT '登录名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `no` varchar(255) DEFAULT NULL COMMENT '工号',
  `code` varchar(255) DEFAULT NULL COMMENT 'code',
  `name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `gender` char(1) DEFAULT NULL COMMENT '性别',
  `status` char(1) DEFAULT NULL COMMENT '状态',
  `email` varchar(128) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(64) DEFAULT NULL COMMENT '手机',
  `user_type` char(1) DEFAULT NULL COMMENT '用户类型',
  `login_ip` varchar(255) DEFAULT NULL COMMENT '最后登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆日期',
  `photo` varchar(255) DEFAULT NULL COMMENT '头像',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单管理';


-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';


-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';


-- ----------------------------
-- Table structure for `sys_dict`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint(20) NOT NULL,
  `value` varchar(100) NOT NULL COMMENT '键值',
  `label` varchar(100) NOT NULL COMMENT '标签名',
  `type` varchar(100) NOT NULL COMMENT '类型',
  `description` varchar(100) NOT NULL COMMENT '描述',
  `sort` decimal(10) DEFAULT NULL COMMENT '排序',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';


-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `username` varchar(64) NOT NULL COMMENT '用户名',
  `operation` varchar(128) DEFAULT NULL COMMENT '用户操作',
  `time` bigint(20) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(255) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `url` varchar(256) DEFAULT NULL COMMENT '地址',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `sys_app_exception`
-- ----------------------------
DROP TABLE IF EXISTS `sys_app_exception`;
CREATE TABLE `sys_app_exception` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `code` varchar(10) DEFAULT NULL COMMENT '异常代码',
  `message` varchar(2000) DEFAULT NULL COMMENT '异常信息',
  `exception` text COMMENT '异常堆栈',
  `version` varchar(255) DEFAULT NULL COMMENT '移动应用版本',
  `phone_info` varchar(255) DEFAULT NULL COMMENT '移动设备信息',
  `create_exception_time` datetime DEFAULT NULL COMMENT '移动端发生异常时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='移动应用异常信息表';


-- ----------------------------
-- Table structure for `sys_tag`
-- ----------------------------
DROP TABLE IF EXISTS `sys_tag`;
CREATE TABLE `sys_tag` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT '标签名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签';


-- ----------------------------
-- Table structure for `sys_tag_office`
-- ----------------------------
DROP TABLE IF EXISTS `sys_tag_office`;
CREATE TABLE `sys_tag_office` (
  `id` bigint(20) NOT NULL,
  `tag_id` varchar(64) NOT NULL COMMENT '标签ID',
  `office_id` varchar(64) NOT NULL COMMENT '机构ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签机构关系表';


-- ----------------------------
-- Table structure for `sys_tag_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_tag_user`;
CREATE TABLE `sys_tag_user` (
  `id` bigint(20) NOT NULL,
  `tag_id` varchar(64) NOT NULL COMMENT '标签ID',
  `user_id` varchar(64) NOT NULL COMMENT '用户ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签用户关系表';


-- ----------------------------
-- Table structure for `sys_gen`
-- ----------------------------
DROP TABLE IF EXISTS `sys_gen`;
CREATE TABLE `sys_gen` (
  `id` bigint(20) NOT NULL,
  `entity_name` varchar(64) DEFAULT NULL COMMENT '实体名',
  `table_name` varchar(64) DEFAULT NULL COMMENT '表名',
  `package_name` varchar(64) DEFAULT NULL COMMENT '包名前缀',
  `module` varchar(64) DEFAULT NULL COMMENT '标签名称',
  `permission` varchar(128) DEFAULT NULL COMMENT '权限标识',
  `file_prefix` varchar(128) DEFAULT NULL COMMENT '文件保存前缀路径',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) DEFAULT NULL COMMENT '是否删除  1：已删除  0：正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `modified_by` bigint(20) DEFAULT NULL COMMENT '修改者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码生成配置表';