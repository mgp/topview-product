java精通计划2
	1，集合框架
		重点掌握ArrayList、LinkedList、Vector、Stack、PriorityQueue、HashSet、
		LinkedHashSet、TreeSet、HashMap、LinkedHashMap、TreeMap、WeakHashMap、
		EnumMap、TreeMap、HashTable的特点和实现原理，还有多线程使用场景和插入多还是删除多的时候使用
		什么样的集合类效率会更高。
	2，IO流
		主要掌握Reader、Writer、InputStream、OutputStream的继承类，
		重点掌握字节流(FileInputStream、FileOutputSteam、BufferedInputStream、
		BufferedOutputStream、DataInputStream、DataOutputStream)和字符流(FileReader、FileWriter、BufferedReader、
		BufferedWriter、InputStreamReader、OutputStreamWriter)，并熟练运用。还有就是NIO的工作原理要知道。
	3，String先关
		主要考察对String的处理，顺带考察多线程和算法，大公司就是喜欢考察字符串的算法，
		主要是字符串查找或者剔除，多线程打印字符串，超大字符串处理。
	4，异常处理
		掌握Throwable继承类，理解Error和Exception的区别，理解运行时异常和编译异常的区别，
		掌握异常的处理方法，掌握try、catch、finally、throw、throws关键字的意义，
		掌握try-catch-finally语句的执行顺序，掌握异常的工作原理，知道常见的异常有哪些。
	5，多线程
		如何创建和启动一个线程，有哪些方法？多线程如何交互，线程之间的调度、让步、合并，如何同步？
		生产和消费者模型。还有就是掌握线程安全、线程池，死锁。
	6，JVM
		掌握常见的垃圾回收机制，掌握程序计数器、堆、栈、方法区的作用，掌握类的加载机制。掌握内存分代模型和工作原理。
		掌握JVM启动参数常见配置，了解JVM调优。
	7，数据结构与算法
		掌握常见查找和排序算法实现及其时间、空间复杂度。掌握常见数据结构如链表、队列、栈的基本原理和实现。
		
		
		
		
		
		
		