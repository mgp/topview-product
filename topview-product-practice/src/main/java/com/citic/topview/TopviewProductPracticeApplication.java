package com.citic.topview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopviewProductPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopviewProductPracticeApplication.class, args);
	}

}
