package com.citic.topview.practice.section1.corejava.socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class NIOServerSocket {
	
	private final static List<SelectionKey> writeQueen = new ArrayList<SelectionKey>();
	
	private static Selector selector = null;
	
    public static void addWriteQueen(SelectionKey key){
        synchronized (writeQueen) {
            writeQueen.add(key);
            selector.wakeup();		// 唤醒主线程
        }
    }

	public static void main(String[] args) throws IOException {
		ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
		serverSocketChannel.bind(new InetSocketAddress(8989));
		serverSocketChannel.configureBlocking(false);														// 设置为非阻塞
		selector = Selector.open();
		
		serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
		
		while (true) {
			int num = selector.select(); 														// 获取可用I/O通道,获得有多少可用的通道
			if(num > 0) {
				Set<SelectionKey> selectedKeys = selector.selectedKeys();
				Iterator<SelectionKey> iterator = selectedKeys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    iterator.remove();															// 调用iterator的remove()方法，并不是移除当前I/O通道，标识当前I/O通道已经处理。
                    
                    if (key.isAcceptable()) {													// 判断事件类型，做对应的处理
                    	ServerSocketChannel ssChannel = (ServerSocketChannel) key.channel();
                    	SocketChannel socketChannel = ssChannel.accept();
                    	
                    	System.out.println("处理请求：" + socketChannel.getRemoteAddress());
                    	
                    	socketChannel.configureBlocking(false);									// 设置为非阻塞	
                    	socketChannel.register(selector, SelectionKey.OP_READ);					// 注册到selector(通道选择器)
                    } else if(key.isReadable()) {
                        key.cancel();															// 取消读事件的监控
                        RequestProcessor.processorRequest(key,selector);						// 调用读操作工具类
                    } else if(key.isWritable()) {
                        key.cancel();															// 取消写事件的监控
                        ResponeProcessor.processorRespone(key);									// 调用写操作工具类
                    }
                }
			} else {
                synchronized (writeQueen) {
                    while(writeQueen.size() > 0){
                        SelectionKey key = writeQueen.remove(0);
                        SocketChannel channel = (SocketChannel) key.channel();					// 注册写事件
                        Object attachment = key.attachment();
                        System.err.println(key);
                        channel.register(selector, SelectionKey.OP_WRITE, attachment);
                    }
                }
            }
		}
	}
}


















