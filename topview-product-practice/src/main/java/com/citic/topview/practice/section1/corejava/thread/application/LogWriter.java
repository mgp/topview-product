package com.citic.topview.practice.section1.corejava.thread.application;

import java.io.PrintWriter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class LogWriter {

	private final BlockingQueue<String> queue;
	private final LoggerThread logger;
	
	boolean shutdownRequested = false;
	
	public LogWriter(PrintWriter writer) {
		this.queue = new LinkedBlockingQueue<String>();
		this.logger = new LoggerThread(writer);
	}
	
	public void start() {
		logger.start();
	}
	
	/**
	 * 不能关闭线程
	 * @param msg
	 * @throws InterruptedException
	 */
/*	public void log(String msg) throws InterruptedException {
		queue.put(msg);
	}*/
	
	/**
	 * 不可靠的关闭
	 * @param msg
	 * @throws InterruptedException
	 */
	public void log(String msg) throws InterruptedException {
		if(!shutdownRequested) {
			queue.put(msg);
		} else {
			throw new IllegalStateException();
		}
	}
	
	class LoggerThread extends Thread{
		
		private final PrintWriter writer;
		
		public LoggerThread(PrintWriter writer) {
			this.writer = writer;
		}

		@Override
		public void run() {
			try {
				while (true) {
					writer.println(queue.take());
				}
			} catch (InterruptedException e) {
				
			} finally {
				writer.close();
			}
		}
	}
}
