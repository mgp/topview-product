package com.citic.topview.practice.section1.corejava.thread;

public class LoggingWidget extends Widget{

	@Override
	public synchronized void doSomething() {
		System.err.println(toString() + ": calling doSomething");
		super.doSomething();
	}
}
