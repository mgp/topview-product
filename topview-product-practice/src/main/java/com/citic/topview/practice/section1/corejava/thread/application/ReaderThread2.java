package com.citic.topview.practice.section1.corejava.thread.application;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class ReaderThread2 extends Thread{

	private final Socket socket;
	private final InputStream in;
	
	public ReaderThread2(Socket socket) throws IOException {
		this.socket = socket;
		this.in = socket.getInputStream();
	}
	
	@Override
	public void run() {
		try {
			byte[] buffer = new byte[1024];
			while (true) {
				int count = in.read(buffer);
				if(count < 0) {
					break;
				} else if(count > 0) {
					processBuffer(buffer, count);
				}
			}
		} catch (Exception e) {

		}
	}
	
	@Override
	public void interrupt() {
		try {
			socket.close();
		} catch (Exception e) {
			
		} finally {
			super.interrupt();
		}
	}
	
	private void processBuffer(byte[] buffer, int count) {
		interrupt();
	}
}
