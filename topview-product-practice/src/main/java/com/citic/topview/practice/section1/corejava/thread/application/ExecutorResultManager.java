package com.citic.topview.practice.section1.corejava.thread.application;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

public class ExecutorResultManager {

	static BlockingQueue<Future<String>> queue = new LinkedBlockingQueue<>();
	
	public static void main(String[] args) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				ExecutorService pool = Executors.newCachedThreadPool();
				for (int i = 0; i < 10; i++) {
					int index = i;
					Future<String> future = pool.submit(new Callable<String>() {
						@Override
						public String call() throws Exception {
							return "task done" + index;
						}
					});
					
					try {
						queue.put(future);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
				}				
			}
		}).start();
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					for (Future<String> future : queue) {
                        if(future.isDone()) {
                            // 处理业务
                            // .............
                        };
                    }
				}
			}
		}).start();
	}
}
