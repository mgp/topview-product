package com.citic.topview.practice.section1.corejava.thread;

public class NoVisibility {

	private static boolean ready = false;
	private static int number = 0;
	
	private static class ReaderThread extends Thread{
		@Override
		public void run() {
			while(ready == false) {
				// yield意味着放手，放弃，投降。一个调用yield()方法的线程告诉虚拟机它乐意让其他线程占用自己的位置。这表明该线程没有在做一些紧急的事情。
				Thread.yield();
			}
			
			System.err.println(number);
		}
	}
	
	
	public static void main(String[] args) {
		new ReaderThread().start();
		number = 42;
		ready = true;
	}
}
