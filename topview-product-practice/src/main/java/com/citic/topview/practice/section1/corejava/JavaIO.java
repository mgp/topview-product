package com.citic.topview.practice.section1.corejava;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class JavaIO {

	/**
	 * 流(https://blog.csdn.net/m0_37955444/article/details/79607883)
	 * 		在Java IO中，流是一个核心的概念。流从概念上来说是一个连续的数据流。你既可以从流中读取数据，
	 * 		也可以往流中写数据。流与数据源或者数据流向的媒介相关联。在Java IO中流既可以是字节流(以字节为单位进行读写)，
	 * 		也可以是字符流(以字符为单位进行读写)。
	 * 	IO相关的媒介
	 * 		Java的IO包主要关注的是从原始数据源的读取以及输出原始数据到目标媒介。以下是最典型的数据源和目标媒介
	 * 			文件
	 * 			管道
	 * 			网络连接
	 * 			内存缓存
	 * 			System.in, System.out, System.error
	 * Java IO类库的框架
	 * 	Java IO的类型
	 * 		虽然java IO类库庞大，但总体来说其框架还是很清楚的。从是读媒介还是写媒介的维度看，Java IO可以分为
	 * 			输入流：InputStream和Reader
	 *			输出流：OutputStream和Writer
	 *		而从其处理流的类型的维度上看，Java IO又可以分为：
	 *			字节流：InputStream和OutputStream
	 *			字符流：Reader和Writer
	 *		我们的程序需要通过InputStream或Reader从数据源读取数据，然后用OutputStream或者Writer将数据写入到目标媒介中。其中，
	 *		InputStream和Reader与数据源相关联，OutputStream和writer与目标媒介相关联
	 * 	IO 类库
	 * 		上面我们介绍了Java IO中的四各类：InputStream、OutputStream、Reader、Writer，其实在我们的实际应用中，我们用到的一般是它们的子类，
	 * 		之所以设计这么多子类，目的就是让每一个类都负责不同的功能，以方便我们开发各种应用。各类用途汇总如下：
	 * 			文件访问
	 * 			网络访问
	 * 			内存缓存访问
	 * 			线程内部通信
	 * 			缓冲
	 * 			过滤
	 * 			解析
	 * 			读写文本
	 * 			读写基本数据类型
	 * 			读写对象
	 * Java IO的基本用法
	 * 	Java IO ：字节流
	 * 		通过上面的介绍我们已经知道，字节流对应的类应该是InputStream和OutputStream，而在我们实际开发中，我们应该根据不同的媒介类型选用相应的子类来处理。下面我们就用字节流来操作文件媒介
	 * 			用字节流写文件
	 * 			String hello = new String( "hello word!");
     *    		byte[] byteArray = hello.getBytes();
     *   		File file = new File( "d:/test.txt");
	 * 			OutputStream os = new FileOutputStream( file);
     *    		os.write( byteArray);
     *   		os.close();
     *   
     *   		用字节流读文件
     *   		File file = new File( "d:/test.txt");
	 * 			byte[] byteArray = new byte[( int) file.length()];
	 * 			InputStream is = new FileInputStream(file);
	 * 			int size = is.read(byteArray);
	 * 			System. out.println( "大小:"+size +";内容:" +new String(byteArray));
	 * 			is.close();
	 * 	Java IO ：字符流
	 * 		字符流对应的类应该是Reader和Writer。下面我们就用字符流来操作文件媒介
	 * 			用字符流写文件
	 * 			String hello = new String("hello word!");
     *   		File file = new File( "d:/test.txt");
     *    		//因为是用字符流来读媒介，所以对应的是Writer，又因为媒介对象是文件，所以用到子类是FileWriter
     *   		Writer os = new FileWriter(file);
     *    		os.write(hello);
     *    		os.close();
     *   
	 * 			用字符流读文件
	 * 			File file = new File("d:/test.txt");
	 * 			Reader reader = new FileReader( file);
	 * 			char [] byteArray = new char[(int) file.length()];
	 * 			int size = reader.read(byteArray);
	 * 			System. out.println("大小:" + size + ";内容:" + new String(byteArray));
	 * 			reader.close();
	 * 	Java IO ：字节流转换为字符流
	 * 		字节流可以转换成字符流，java.io包中提供的InputStreamReader类就可以实现，当然从其命名上就可以看出它的作用。其实这涉及到另一个概念，
	 * 		IO流的组合，后面我们详细介绍。下面看一个简单的例子
     *   		File file = new File( "d:/test.txt");
	 * 			InputStream is = new FileInputStream(file);
	 * 			Reader reader = new InputStreamReader(is);
	 * 			char [] byteArray = new char[(int) file.length()];
	 * 			int size = reader.read(byteArray);
	 * 			is.close();
	 * 			reader.close();
	 * 	Java IO ：IO类的组合
	 * 		从上面字节流转换成字符流的例子中我们知道了IO流之间可以组合（或称嵌套），其实组合的目的很简单，就是把多种类的特性融合在一起以实现更多的功能。
	 * 		组合使用的方式很简单，通过把一个流放入另一个流的构造器中即可实现，两个流之间可以组合，三个或者更多流之间也可组合到一起。当然，并不是任意流之间都可以组合。
	 * 		关于组合就不过多介绍了，后面的例子中有很多都用到了组合，大家好好体会即可。
	 * 	Java IO：文件媒介操作
	 * 		File是Java IO中最常用的读写媒介，那么我们在这里就对文件再做进一步介绍
	 * 		File媒介
	 * 		随机读取File文件
	 * 			通过上面的例子我们已经知道，我们可以用FileInputStream（文件字符流）或FileReader（文件字节流）来读文件，
	 * 			这两个类可以让我们分别以字符和字节的方式来读取文件内容，但是它们都有一个不足之处，就是只能从文件头开始读，然后读到文件结束。	
	 * 			但是有时候我们只希望读取文件的一部分，或者是说随机的读取文件，那么我们就可以利用RandomAccessFile。RandomAccessFile提供了seek()方法，
	 * 			用来定位将要读写文件的指针位置，我们也可以通过调用getFilePointer()方法来获取当前指针的位置，具体看下面的例子:
	 * 
	 * 			随机读取文件
	 * 			RandomAccessFile file = new RandomAccessFile( "d:/test.txt", "rw");
	 * 			file.seek(10);
	 * 			long pointerBegin = file.getFilePointer();
	 * 			byte[] contents = new byte[1024];
	 * 			file.read(contents);
	 * 			long pointerEnd = file.getFilePointer();
	 * 			file.close();
	 * 
	 * 			随机写入文件
	 * 			RandomAccessFile file = new RandomAccessFile( "d:/test.txt", "rw");
	 * 			file.seek(10);
	 * 			long pointerBegin = file.getFilePointer();
	 * 			file.write( "HELLO WORD".getBytes());
	 * 			long pointerEnd = file.getFilePointer();
	 * 			file.close();
	 * 	Java IO：管道媒介
	 * 		管道主要用来实现同一个虚拟机中的两个线程进行交流。因此，一个管道既可以作为数据源媒介也可作为目标媒介。
	 * 		需要注意的是java中的管道和Unix/Linux中的管道含义并不一样，在Unix/Linux中管道可以作为两个位于不同空间进程通信的媒介，而在java中，
	 * 		管道只能为同一个JVM进程中的不同线程进行通信。和管道相关的IO类为：PipedInputStream和PipedOutputStream，下面我们来看一个例子：
	 * 			见方法pipedReaderWriter
	 * 	Java IO：网络媒介
	 * 		关于Java IO面向网络媒介的操作即Java 网络编程，其核心是Socket，同磁盘操作一样，java网络编程对应着两套API，即Java IO和Java NIO，关于这部分我会准备专门的文章进行介绍。
	 * 	Java IO：BufferedInputStream和BufferedOutputStream
	 * 		BufferedInputStream顾名思义，就是在对流进行写入时提供一个buffer来提高IO效率。在进行磁盘或网络IO时，
	 * 		原始的InputStream对数据读取的过程都是一个字节一个字节操作的，而BufferedInputStream在其内部提供了一个buffer，在读数据时，
	 * 		会一次读取一大块数据到buffer中，这样比单字节的操作效率要高的多，特别是进程磁盘IO和对大量数据进行读写的时候
	 * 		使用BufferedInputStream十分简单，只要把普通的输入流和BufferedInputStream组合到一起即可
	 * 			File file = new File("d:/test.txt");
	 * 			byte[] byteArray = new byte[(int) file.length()];
	 * 			InputStream is = new BufferedInputStream(new FileInputStream(file), 2*1024);
	 * 			int size = is.read(byteArray);
	 * 			System.out.println("大小:" + size + ";内容:" + new String(byteArray));
	 * 			is.close();
	 * 		关于如何设置buffer的大小，我们应根据我们的硬件状况来确定。对于磁盘IO来说，如果硬盘每次读取4KB大小的文件块，那么我们最好设置成这个大小的整数倍。
	 * 		因为磁盘对于顺序读的效率是特别高的，所以如果buffer再设置的大写可能会带来更好的效率，比如设置成4*4KB或8*4KB。
	 * 		BufferedOutputStream的情况和BufferedInputStream一致，在这里就不多做描述了。
	 * 	Java IO：BufferedReader和BufferedWriter
	 * 		BufferedReader、BufferedWriter 的作用基本和BufferedInputStream、BufferedOutputStream一致，具体用法和原理都差不多 ，
	 * 		只不过一个是面向字符流一个是面向字节流。同样，我们将改造字符流中的例4，给其加上buffer功能，看例子
	 * 			File file = new File("d:/test.txt");
	 * 			Reader reader = new BufferedReader(new FileReader(file), 2*1024);
	 * 			char[] byteArray = new char[(int)file.length()];
	 * 			int size = reader.read( byteArray);
	 * 			reader.close();
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

	}
	
	public void pipedReaderWriter() throws IOException {
		final PipedOutputStream output = new PipedOutputStream();
        final PipedInputStream  input  = new PipedInputStream(output);
        
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    output.write("Hello world, pipe!".getBytes());
                } catch (IOException e) {
                }
            }
        });
        
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int data = input.read();
                    while( data != -1){
                        System. out.print((char)data);
                        data = input.read();
                    }
                } catch (IOException e) {
                	
                } finally{
                   try {input.close();} catch (IOException e) {e.printStackTrace();}
                }
            }
        });
        
        thread1.start();
        thread2.start();
	}
}
