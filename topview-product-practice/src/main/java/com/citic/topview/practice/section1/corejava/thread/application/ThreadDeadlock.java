package com.citic.topview.practice.section1.corejava.thread.application;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ThreadDeadlock {

	ExecutorService executorService = Executors.newSingleThreadExecutor();

	public class RenderPageTask implements Callable<String> {
		@Override
		public String call() throws Exception {
			Future<String> header, footer;
			header = executorService.submit(new LoadFileTask("header.html"));
			footer = executorService.submit(new LoadFileTask("footer.html"));
			String page = renderPage();
			return header.get() + page + footer.get();
		}
	}

	public String renderPage() {
		return null;
	}
}

class LoadFileTask implements Callable<String>{

	public LoadFileTask(String filename){
		
	}
	
	@Override
	public String call() throws Exception {
		
		return null;
	}
}