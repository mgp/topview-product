package com.citic.topview.practice.section1.corejava.thread.application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LifecycleWebServer {

	private static final int thread_max_active = 100;
	
	private static final ExecutorService executor = Executors.newFixedThreadPool(thread_max_active);
	
	private static ServerSocket serverSocket;
	
	public void start() throws IOException {
		serverSocket = new ServerSocket(8088);
		while(!executor.isShutdown()) {
			final Socket socket = serverSocket.accept();
			executor.execute(new Runnable() {
				@Override
				public void run() {
					handleRequest(socket);
				}
			});
		}
	}
	
	public void stop() {
		executor.shutdown();
	}
	
	protected static void handleRequest(Socket socket) {
		
	}
}
