package com.citic.topview.practice.section1.corejava.thread.application;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class SocketUsing extends SocketUsingTask<Socket>{

	private final Socket socket;
	private final InputStream in;
	
	public SocketUsing(Socket socket) throws IOException {
		this.socket = socket;
		this.in = this.socket.getInputStream();
	}
	
	@Override
	public Socket call() throws Exception {
		try {
			byte[] buffer = new byte[1024];
			while (true) {
				int count = in.read(buffer);
				if(count < 0) {
					break;
				} else if(count > 0) {
					processBuffer(buffer, count);
				}
			}
		} catch (Exception e) {

		}
		return this.socket;
	}

	private void processBuffer(byte[] buffer, int count) {
		
	}
	
	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
		BlockingQueue<Runnable> blockingQueue = new LinkedBlockingQueue<>();
		ExecutorService executorService = new CancelExecutor(3, 3, 100, TimeUnit.SECONDS, blockingQueue);
		
		SocketUsing socketUsing = new SocketUsing(new Socket());
		Future<Socket> future = executorService.submit(socketUsing);
		
		future.cancel(true);
	}
}
