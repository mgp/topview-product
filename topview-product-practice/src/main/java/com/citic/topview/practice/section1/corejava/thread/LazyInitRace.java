
package com.citic.topview.practice.section1.corejava.thread;

import com.citic.topview.practice.section1.corejava.thread.annotation.ThreadSafe;

@ThreadSafe
public class LazyInitRace {

	private JavaThread1 instance = null;
	
	public JavaThread1 getInstance() {
		synchronized (this) {
			if(instance == null)
				instance = new JavaThread1();
		}
		
		return instance;
	}
}
