package com.citic.topview.practice.section1.corejava.thread.application;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableFuture {

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newCachedThreadPool();
		
		Task task = new Task();
		Future<Integer> result = executorService.submit(task);
		executorService.shutdown();
		
		try {
			Thread.sleep(1000);
		} catch (Exception e) {

		}
		
		try {
			System.err.println(result.get());
		} catch (Exception e) {

		}
	}
}

class Task implements Callable<Integer>{

	@Override
	public Integer call() throws Exception {
		Thread.sleep(3000);
		
		int sum = 0;
		for (int i = 0; i < 100; i++) {
			sum ++;
		}
		return sum;
	}
}
