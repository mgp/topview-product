package com.citic.topview.practice.section1.corejava.socket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class NIOClientSocket {

	public static void main(String[] args) throws IOException {
        for (int i = 0; i < 2; i++) {															// 使用线程模拟用户 并发访问
            new Thread(){
                public void run() {
                    try {
                        SocketChannel socketChannel=SocketChannel.open();						// 1.创建SocketChannel
                        socketChannel.connect(new InetSocketAddress("127.0.0.1", 8989));		// 2.连接服务器

                        String msg = "我是客户端" + Thread.currentThread().getId();					// 写数据		
                        ByteBuffer buffer = ByteBuffer.allocate(1024);
                        buffer.put(msg.getBytes());
                        buffer.flip();
                        socketChannel.write(buffer);
                        socketChannel.shutdownOutput();

                        //读数据
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        int len = 0;
                        while (true) {
                            buffer.clear();
                            len = socketChannel.read(buffer);
                            if (len == -1)
                                break;
                            
                            buffer.flip();
                            while (buffer.hasRemaining()) {
                                bos.write(buffer.get());
                            }
                        }

                        System.out.println("客户端收到:" + new String(bos.toByteArray()));

                        socketChannel.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                };
            }.start();
        }
    }
}
