package com.citic.topview.practice.section1.corejava;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class JavaCollection {

	/**
	 * https://blog.csdn.net/u011552404?t=1
	 * Arraylist
	 * 		ArrayList是List接口的可变数组的实现。实现了所有可选列表操作，并允许包括 null 在内的所有元素。除了实现 List 接口外，此类还提供一些方法来操作内部用来存储列表的数组的大小。
	 * 		对于ArrayList而言，它实现List接口、底层使用数组保存所有元素。其操作基本上是对数组的操作。下面我们来分析ArrayList的源代码
	 * 		ArrayList可以指定初始容量的空列表
	 * 	构造方法
	 * 		public ArrayList()
	 * 		public ArrayList(int initialCapacity)
	 * 		public ArrayList(Collection<? extends E> c)
	 * 		其中第一种方法默认构造数组大小是0，只有在添加第一个数据的时候才自动扩容至默认大小10；
	 * 		第二种构造器是自定义默认大小；
	 * 		第三种构造器即传入集合进行初始化，因为ArrayList的底层是数组实现，所以实际的初始化原理是首先把集合转变成Array类然后再通过深拷贝的方法初始化ArrayList！
	 * 	扩容机制
	 * 		扩容通常发生在添加元素的时候，发现超过当前数组大小（起初默认是10）的时候。其中默认扩容大小是1.5倍。由于
	 * 		ArrayList底层的数组本身并不是动态的，所以扩容之后并不是直接在数组后添加空位置，进而赋值，而是重新深拷贝一个
	 * 		数组长度为当前数组的1.5倍的新数组，然后再添加新的元素！其中扩容源码如下
	 * 	为什么增删操作的效率低
	 * 		举例只讲插入的话
	 * 		如果只是插入尾节点且没有空间足够，其实直接赋值就可以，和链表的复杂度一样
	 * 		超过当前空间（不论是尾插还是中间插入），则需要扩容！因为上文已经说过，扩容会导致深拷贝一个新的数组，那么这个复杂度就是n了！ 
	 * 		没有超过当前空间，但是在中间插入，假设插入位置为index，则插入操作要使得index后的数据全部往后移动一格——需要注意的是，这个移动一格还是通过深拷贝来实现的
	 * LinkedList
	 * 		LinkedList 是一个继承于AbstractSequentialList的双向链表。它也可以被当作堆栈、队列或双端队列进行操作。
	 *		LinkedList 实现 List 接口，能对它进行队列操作。
	 *		LinkedList 实现 Deque 接口，即能将LinkedList当作双端队列使用。
	 *		LinkedList 实现了Cloneable接口，即覆盖了函数clone()，能克隆。
	 *		LinkedList 实现java.io.Serializable接口，这意味着LinkedList支持序列化，能通过序列化去传输。
	 *		LinkedList 是非同步的。	
	 * HashMap
	 * 		HashMap基于哈希表的 Map 接口的实现。此实现提供所有可选的映射操作，并允许使用 null 值和 null键。（除了不同步和允许使用 null 之外，HashMap 类与 Hashtable大致相同） 
	 * 		此类不保证映射的顺序，特别是它不保证该顺序恒久不变。 
	 * 		值得注意的是HashMap不是线程安全的，如果想要线程安全的HashMap，可以通过Collections类的静态方法  Map map = Collections.synchronizedMap(new HashMap());
	 * 	HashMap的底层结构
	 * 		hashMap说到底还是hash表，默认的初始化大小为16，且保证为2的n次方！
	 * 		hash表本质上还是采用的数组来构造的！但是在存数据的时候会出现hash冲突，所以为了解决冲突主要出现了两种方式(开放地址法, 链地址法),HashMap正是采用第二种方式！但是如果冲突很多会出现链表越来越长，所以在JDK8的时候，为了提高效率当一个位置的链表节点大于8，将把链表重新组成红黑树！所以，实际JDK8的数据结构形式是这样的
	 * 	hashMap存取数据的原理
	 * 		首先要明白，不论是数组节点和链表节点、红黑树节点每个节点存放的是一个Node <K,V>对象（注意是对象，不是value基本的数据类型值），
	 * 		而Node<K,V>又继承自Map.Entry<K,V>；实际上HashMap底层维护的是一个Node[] table数组，我们称之为Hash桶数组；
	 * HashTable
	 * 		1、Hashtable和HashMap，从存储结构和实现来讲基本上都是相同的，  Hashtable继承自Dictionary类，而HashMap继承自AbstractMap类，但二者都实现了Map接口。 
	 *		2、它和HashMap的最大的不同是它是线程安全的，另外它不允许key和value为null。 
	 *		3、Hashtable是个过时的集合类，不建议在新代码中使用，不需要线程安全的场合可以用HashMap替换，需要线程安全的场合可以用ConcurrentHashMap替换！
	 * 
	 * Set
	 * 		Set我们众所周知的就是虑重功能， 我们平时在项目开发中也常用到这个特性的。那么Set为何能够虑重呢？ 接下来我们就看下源码吧。
	 *		Set的底层实现是HashMap（这个后面讲Map时也会讲它的源码）， 当我们在HashSet中添加一个新元素时， 其实这个值是存储在底层Map的key中，而众所周知，HashMap的key值是不能重复的， 所以这里就可以达到去重的目的了。
	 * 
	 * Vector
	 * 		同ArrayList，但是Vector是线程同步的，性能没有ArrayList好
	 * Stack
	 * 		继承Vector，也是线程安全的，为了实现后进先出
	 * ArrayList和LinkedList的大致区别如下:
	 *		1.ArrayList是实现了基于动态数组的数据结构，LinkedList基于链表的数据结构。 
	 *		2.对于随机访问get和set，ArrayList觉得优于LinkedList，因为LinkedList要移动指针。 
	 *		3.对于新增和删除操作add和remove，LinedList比较占优势，因为ArrayList要移动数据。
	 * WeakHashMap
	 * 		和HashMap一样，WeakHashMap 也是一个散列表，它存储的内容也是键值对(key-value)映射，而且键和值都可以是null。
	 * 		不过WeakHashMap的键是“弱键”。在 WeakHashMap 中，当某个键不再正常使用时，会被从WeakHashMap中被自动移除。
	 * 		更精确地说，对于一个给定的键，其映射的存在并不阻止垃圾回收器对该键的丢弃，这就使该键成为可终止的，被终止，然后被回收。某个键被终止时，它对应的键值对也就从映射中有效地移除了。
	 * 		和HashMap一样，WeakHashMap是不同步的。可以使用 Collections.synchronizedMap 方法来构造同步的 WeakHashMap。
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		
		System.err.println(list);
		
		int[] a = {1, 2};
		int[] c = Arrays.copyOf(a, a.length);
		System.err.println(a == c);
		
		LinkedList<String> linkedList = new LinkedList<>();
		linkedList.addFirst("1");
		linkedList.addFirst("2");
		for (String string : linkedList) {
			System.err.println(string);
		}
	}
}
