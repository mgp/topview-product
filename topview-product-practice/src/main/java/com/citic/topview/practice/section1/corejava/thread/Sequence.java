package com.citic.topview.practice.section1.corejava.thread;

import com.citic.topview.practice.section1.corejava.thread.annotation.GuardeBy;
import com.citic.topview.practice.section1.corejava.thread.annotation.ThreadSafe;

@ThreadSafe
public class Sequence {

	@GuardeBy("this")private int value;
	
	public synchronized int getNext() {
		return value ++;
	}
}
