package com.citic.topview.practice.section1.corejava.thread.application;

import java.io.PrintWriter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class LogWriter3 {

	private final BlockingQueue<String> queue;
	private final PrintWriter writer;
	private final LoggerThread logger;
	
	private final ExecutorService executorService = Executors.newFixedThreadPool(30);
	
	public LogWriter3(PrintWriter writer) {
		this.queue = new LinkedBlockingQueue<String>();
		this.writer = writer;
		this.logger = new LoggerThread();
	}
	
	public void start() {
		logger.start();
	}
	
	public void stop() throws InterruptedException {
		try {
			executorService.shutdown();
			executorService.awaitTermination(10, TimeUnit.SECONDS);
		} finally {
			writer.close();
		}
	}
	
	public void log(String msg) throws InterruptedException {
		try {
			executorService.execute(new WriterTask(msg));
		} catch (Exception e) {
		}
	}
	
	class WriterTask implements Runnable{

		private final String msg;
		
		public WriterTask(String msg) {
			this.msg = msg;
		}
		
		@Override
		public void run() {
			try {
				queue.put(msg);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	class LoggerThread extends Thread{
		@Override
		public void run() {
			try {
				while (true) {
					writer.println(queue.take());				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				writer.close();
			}
		}
	}
}
