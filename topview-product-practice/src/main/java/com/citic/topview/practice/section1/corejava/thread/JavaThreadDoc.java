package com.citic.topview.practice.section1.corejava.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class JavaThreadDoc {

	/**
	 * 概念定义
	 * 	竞态条件
	 * 		数据计算，读取数值，增加数值和回写数值，其中每一步都是可以被打断和暂停的，多线程访问数值变量就是可能会造成问题。这是一种由于不恰当的执行顺序而造成的多线程错误，被称为竞态条件
	 * 	synchronized
	 * 		关键字synchronized不光实现了原子性，还实现内存可见性(Memory Visibility)。也就是在同步的过程中，不仅要防止某个线程正在使用的状态被另一个线程修改，还要保证一个线程修改了对象状态之后，其他线程能获得更新之后的状态。
	 * 	内存可见性
	 * 		在没有同步机制的情况下，在多线程的环境中，每个进程单独使用保存在自己的线程环境中的变量拷贝。正因如此，当多线程共享一个可变状态时，该状态就会有多份拷贝，当一个线程环境中的变量拷贝被修改了，并不会立刻就去更新其他线程中的变量拷贝。
	 * 		因为以上原因，所以内存可见性很重要
	 * 	重排序
	 * 		也就是JVM根据优化的需要调整“不相关”代码的执行顺序。在主线程中，number = 42和ready = true看似是不相关的，不相互依赖，所以可能被JVM在编译时颠倒执行顺序，所以才会出现这个奇怪结果
	 * 	重排序和变量多拷贝
	 * 		可能看上去是一种奇怪的设计，但是这样做的目的是希望JVM能充分利用多核处理器强大的性能，Java内存模型更为具体的内容将会在未来的篇章中为大家详细介绍。
	 * 	Volatile变量
	 * 		加锁当然是多线程安全的完备方法，但是有的时候只需要确保少数状态变量的可见性即可，使用加锁机制未免有些大材小用，因此Java语言提供一种稍弱的同步机制——Volatile变量。
	 * 		当变量被声明为Volatile类型后，在编译时和运行时，JVM都会注意到这是一个共享变量，既不会在编译时对该变量的操作进行重排序，也不会缓存该变量到其他线程不可见的地方，保证所有线程都能读取到该变量的最新状态。
	 * 		访问Volatile变量时并没使用加锁操作，不会阻塞线程的运行，所以性能远远优于同步代码块和上锁机制，只比访问正常变量略高，不过这是牺牲原子性为代价的。
	 * 		加锁机制可以确保可见性、原子性和不可重排序性，但是Volatile变量只能确保可见性和不可重排序性。
	 * 
	 * 	发布
	 * 		发布对象意味着该对象能在当前作用域之外的代码中被使用，比如，将类内部的对象传给其他类使用，或者一个非私有方法返回了该对象的引用等等。
	 * 		Java中强调类的封装性就是希望能合理的发布对象，保护类的内部信息。发布类内部状态，在多线程的环境下可能问题不大，但是在并发环境中却用可能严重地破坏多线程安全。
	 * 	逸出
	 * 		某个不该发布的对象被发布了，这种情况被称为逸出
	 * 		例子
	 * 			class unsafeState{
	 * 				private String[] states = new String[]{"AK", "AL"}
	 * 				
	 * 				public String[] getStates(){return states;}
	 * 			}
	 * 			上面的例子中，虽然states是私有变量，但是其被共有方法所暴露，数组中的元素都可以被任意修改，这就是一种逸出的情况。
	 * 	隐蔽的this逸出
	 * 		内部的匿名类是隐私持有外部类的this引用的，这就无意中将this发布给内部类，如果内部类再被发布，则外部类就可能逸出，无意间造成内存泄漏和多线程安全问题。
	 * 		具体来说，只有当构造器执行结束后，this对象完成初始化后才能发布，否者就是一种不正确的构造，存在多线程安全隐患。
	 * 		
	 * 		解决这个问题最常见的方法就是工厂模式：
	 * 			public class SafeListener {
	 *			    private final EventListener listener;
	 *			
	 *			    private SafeListener() {
	 *			        listener = new EventListener() {
	 *			            public void onEvent(Event e) {
	 *			                doSomething(e);
	 *			            }
	 *			        };
	 *			    }
	 *			
	 *			    public static SafeListener newInstance(EventSource source) {
	 *			        SafeListener safe = new SafeListener();
	 *			        source.registerListener(safe.listener);
	 *			        return safe;
	 *			    }
	 *			}
	 * 			上例中，外部类的构造器被设置为私有的，其他类执行外部类的公有静态方法在构造器执行完毕之后才返回对象的引用，避免了this对象的逸出问题。 相对而言，对象安全发布的问题比可见性问题更容易被忽视，接下来就讨论下如何才能安全发布对象。	
	 * 	线程封闭
	 * 		对象的发布既然是个头疼的问题，所以我们应该避免泛滥地发布对象，最简单的方式就是尽可能把对象的使用范围都控制在单线程环境中，也就是线程封闭。
	 * 		方法
	 * 			Ad-hoc线程封闭，也就是维护线程封闭性的责任完全由编程承担，这种方法是不推荐的；
	 * 			局部变量封闭，很多人容易忽视一点，局部变量的固有属性之一就是封闭在执行线程内，无法被外界引用，所以尽量使用局部变量可以减少逸出的发生；
	 * 			ThreadLocal，这是一种更为规范的方法，该类将把进程中的某个值和保存值的对象关联起来，并提供get和set方法，保证get方法获得的值都是当前进程调用set方法设置的最新值。
	 * 		备注：ThreadLoacl在JDBC和J2EE容器中有着大量的应用。比如，在JDBC中，ThreadLoacl用来保证每个线程只能有一个数据库连接，再如在J2EE中，用以保存线程的上下文，方便线程切换等。
	 * 	对象不可变
	 * 		该对象是正确创建的，没有this逸出问题
	 * 		该对象的所有状态在创建之后不能修改，也就是其set方法应该为私有的，或者该域直接是final的
	 * 	安全发布
	 * 		在静态初始化函数中初始化一个对象的引用（态初始化函数由JVM在初始化阶段执行，JVM为其提供同步机制）；
	 *		将对象的引用保存在Volatile域或AtomicReference对象中；
	 *		将对象的引用保存在某个正确构造对象的final域中；
	 *		将对象的引用保存到一个由锁保护的域中；
	 *		将对象的引用保存到线程安全容器中；
	 * 	内存模型
	 * 		在生成指令顺序可能和源代码中顺序不相同；
	 * 		编译器可能会把变量保存到寄存器中而非内存中
	 * 		处理器可以采用乱序或者并行的方式执行指令
	 * 		缓存可能会改变将写入变量提交到主内存的次序
	 * 		保存在处理器本地缓存中的值，对于其他处理器是不可见的
	 * 
	 * 		多处理器架构中的内存模型
	 * 			在多核理器架构中，每个处理器都拥有自己的缓存，并且会定期地与主内存进行协调。这样的架构就需要解决缓存一致性（Cache Coherence）的问题。很可惜，一些框架中只提供了最小保证，即允许不同处理器在任意时刻从同一存储位置上看到不同的值。
	 * 			正因此存在上面所述的硬件能力和线程安全需求的差异，才导致需要在代码中使用同步机制来保证多线程安全。
	 * 		重排序
	 *			JVM不光会改变命令执行的顺序，甚至还会让不同线程看到的代码执行的顺序也是不同的，这就会让在没有同步操作的情况下预测代码执行结果边变的困难。
	 * 		Java内存模型与Happens-Before规则
	 * 			https://www.jianshu.com/p/47649b1fdbf8
	 * 			public class ResourceFactory {
	 *			    //静态初始化不需要额外的同步机制
	 *			    private static class ResourceHolder {
	 *			        public static Resource resource = new Resource();
	 *			    }
	 *			
	 *			    //延迟加载
	 *			    public static Resource getResource() {
	 *			        return ResourceHolder.resource;
	 *			    }
	 *			
	 *			    static class Resource {
	 *			    }
	 *			}
	 *	双重检查加锁(臭名昭著)
	 *		示例代码：
	 *			public class DoubleCheckedLocking {
	 *			    private static Resource resource;
	 *			
	 *			    public static Resource getInstance() {
	 *			        //没有在同步的情况下读取共享变量，破坏了Happens_Before规则
	 *			        if (resource == null) {
	 *			            synchronized (DoubleCheckedLocking.class) {
	 *			                if (resource == null)
	 *			                    resource = new Resource();
	 *			            }
	 *			        }
	 *			        return resource;
	 *			    }
	 *			
	 *			    static class Resource {
	 *			
	 *			    }
	 *			}
	 *			
	 *			代码（resource = new Resource()）实际上可以分解成以下三个步骤
	 *				分配内存空间，初始化对象，将对象指向刚分配的内存空间
	 *			但是有些编译器为了性能的原因，可能会将第二步和第三步进行重排序，顺序就成了：
	 *				分配内存空间，将对象指向刚分配的内存空间，初始化对象
	 *
	 *			解释
	 *				线程A，检查到对象为null，获取锁，再次检查到对象为null，为对象分配内存空间，将对象引用指向内存空间，线程B检查到对象不为null并访问（此时对象还未完成初始化），线程A初始化对象
	 *			解决办法
	 *				为对象添加volatile关键字
	 *				枚举实现单例模式是线程安全的，较推荐
	 * 
	 * 	Callable 
	 * 		Callable支持任务有返回值，并支持异常的抛出。如果希望获得子线程的执行结果，那Callable将比Runnable更为合适。
	 * 	Future
	 * 		Future类表示任务生命周期状态，其命名体现了任务的生命周期只能向前不能后退。
	 * 		Future类提供方法查询任务状态外，还提供get方法获得任务的返回值，但是get方法的行为取决于任务状态
	 * 		如果将一个Callable对象提交给ExecutorService，submit方法就会返回一个Future对象，通过这个Future对象就可以在主线程中获得该任务的状态，并获得返回值。
	 * 	FutureTask
	 * 		FutureTask不光继承了Future接口，也继承Runnable接口，所以可以直接调用run方法执行。
	 * 		它既可以作为Runnable被线程执行，又可以作为Future得到Callable的返回值。
	 * 		实例：test方法
	 * 	CompletionService
	 * 		CompletionService可以理解为Executor和BlockingQueue的组合：当一组任务被提交后，CompletionService将按照任务完成的顺序将任务的Future对象放入队列中。
	 * 	关闭线程
	 * 		取消标志位
	 * 			该机制的最大的问题就是无法应用于拥塞方法，假设在循环中调用了拥塞方法，任务可能因拥塞而永远不会去检查取消标志位，甚至会造成永远不能停止
	 * 		中断机制
	 * 			public class Thread {
	 *			    // 中断当前线程
	 *			    public void interrupt();
	 *			    // 判断当前线程是否被中断
	 *			    public boolen isInterrupt();
	 *			    // 清除当前线程的中断状态，并返回之前的值
	 *			    public static boolen interrupt();   
	 *			}	
	 *		定时运行
	 *		通过Future取消任务
	 *			Future用来管理任务的生命周期，自然也可以来取消任务，调用Future.cancel方法就是用中断请求结束任务并退出，这也是Executor的默认中断策略。
	 *		不可中断的拥塞
	 *			一些的方法的拥塞是不能响应中断请求的，这类操作以I/O操作居多，但是可以让其抛出类似的异常，来停止任务
	 *				Socket I/O: 关闭底层socket，所有因执行读写操作而拥塞的线程会抛出SocketException；
	 *				同步 I/O：大部分Channel都实现了InterruptiableChannel接口，可以响应中断请求，抛出异常ClosedByInterruptException;
	 *				Selector的异步 I/O：Selector执行select方法之后，再执行close和wakeUp方法就会抛出异常ClosedSelectorException。
	 *				
	 * 	join
	 * 		t.join()方法阻塞调用此方法的线程(calling thread)，直到线程t完成，此线程再继续；通常用于在main()主线程内，等待其它线程完成再结束main()主线程。
	 * 
	 * 
	 * 
	 * 知识点
	 * 	1，编写多线程完全的代码，最关键的一点就是需要对于共享的和可变的状态进行访问控制
	 * 		所谓共享的，指的是该变量可能同时被多个线程访问
	 * 		所谓可变的，指的是该变量在生命周期内其值可能放生变化
	 * 	2，如何确保多线程安全
	 * 		原子性
	 * 			将一组操作组成一个原子性的复合操作，在复合操作没有执行完之前，该操作过程不能被打断。
	 * 			AtomicLong，Volatile
	 * 		加锁机制
	 *			原因：如果多线程中的共享状态变量有多个，该如何处理呢？只靠每个变量为原子类型是不够的，还需要把所有状态变量之间的操作都设置成原子性的才行
	 *			机制：每个对象内部都会有一个内置锁，当进入同步代码块时，对象的内置锁就会被自动获得，在退出同步代码块（包括抛出异常）都会自动释放内置锁。同步代码块中的程序，将会保证是原子性的，这是因为内置锁是一种互斥锁，每次只能有一个线程获得该锁，从而保证多线程之间相互不干扰。 	
	 * 			说明：内置锁提供重入机制，也就是说如果当前线程已经获得某个对象的内置锁，当它再去请求该锁时也会成功，这就代表着内置锁的操作粒度是线程，而不是调用。
	 * 			
	 * 			重入机制
	 * 				原因：如果没有重入机制，当B类中的function已经获得内置锁之后，再去调用父类中同步方法function，就会因内置锁未获得而等待，但是外部的子类的方法因内部父类的方法被拥塞也一直都不能释放内置锁，故而产生死锁。
	 * 				代码实例
	 * 					public class A{
	 * 						public synchronized void function(){}
	 * 					}
	 * 					public class B extends A{
	 * 						public synchronized void function(){
	 * 							super.function();
	 * 							......
	 * 						}
	 * 					}
	 * 		
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
	}
	
	public void test() {
		ExecutorService executor = Executors.newCachedThreadPool();
		FutureTask<Integer> futureTask = new FutureTask<Integer>(new Callable<Integer>() {
			@Override
			public Integer call() throws Exception {
				int sum = 0;
		        for(int i = 0; i < 100; i++)
		            sum += i;
		        return sum;
			}
		});
		executor.submit(futureTask);
        executor.shutdown();
		
	}
}

class LaunderThrowable {
    public static RuntimeException launderThrowable(Throwable t) {
        if (t instanceof RuntimeException)
            return (RuntimeException) t;
        else if (t instanceof Error)
            throw (Error) t;
        else
            throw new IllegalStateException("Not unchecked", t);
    }
}

class TimedRun {
	private static final ScheduledExecutorService cancelExec = Executors.newScheduledThreadPool(1);
	
	public static void timedRun(final Runnable r, long timeout, TimeUnit unit) throws InterruptedException {
		class RethrowableTask implements Runnable {
	        private volatile Throwable t;
	        public void run() {
	            try {
	                r.run();
	            } catch (Throwable t) {
	                this.t = t; // 中断策略，保存当前抛出的异常，退出
	            }
	        }
	        
	        void rethrow() { // 再次抛出异常
	            if (t != null) throw LaunderThrowable.launderThrowable(t);
	        }
	    }
		
		RethrowableTask task = new RethrowableTask();
		final Thread taskThread = new Thread(task);
        taskThread.start(); // 开启任务子线程
        
        cancelExec.schedule(new Runnable() { // 定时中断任务子线程
            public void run() {
                taskThread.interrupt();
            }
        }, timeout, unit);
        
        taskThread.join(unit.toMillis(timeout)); // 限时等待任务子线程执行完毕
        task.rethrow(); // 尝试抛出task在执行中抛出到异常
	}

}