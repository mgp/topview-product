package com.citic.topview.practice.section1.corejava.thread;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ThreadLocals {

	private static ThreadLocal<Connection> connectionHolder = new ThreadLocal<Connection>() {
		public Connection initialValue() {
			try {
				return DriverManager.getConnection("");
			} catch (SQLException e) {
				return null;
			}
		}
	};
	
	public static Connection getConnection() {
		return connectionHolder.get();
	} 
}
