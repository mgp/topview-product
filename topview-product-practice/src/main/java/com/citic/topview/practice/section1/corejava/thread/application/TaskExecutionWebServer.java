package com.citic.topview.practice.section1.corejava.thread.application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TaskExecutionWebServer {

	private static final int thread_max_active = 100;
	
	private static final Executor executor = Executors.newFixedThreadPool(thread_max_active);
	
	private static ServerSocket serverSocket;
	
	public static void main(String[] args) throws IOException {
		serverSocket = new ServerSocket(8088);
		while(true) {
			final Socket socket = serverSocket.accept();
			Runnable task = new Runnable() {
				@Override
				public void run() {
					handleRequest(socket);
				}
			};
			
			executor.execute(task);
		}
	}

	protected static void handleRequest(Socket socket) {
		
	}
}
