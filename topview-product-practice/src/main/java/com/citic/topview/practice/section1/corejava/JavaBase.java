package com.citic.topview.practice.section1.corejava;

public class JavaBase {

	/**
	 * 1，JVM、JRE和JDK的区别
	 * 		JVM(Java Virtual Machine):java虚拟机，用于保证java的跨平台的特性。java语言是跨平台，jvm不是跨平台的。
	 * 		JRE(Java Runtime Environment):java的运行环境,包括jvm+java的核心类库。
	 * 		JDK(Java Development Kit):java的开发工具,包括jre+开发工具
	 * 2，基本数据类型
	 * 		byte
	 * 			byte 数据类型是8位、有符号的，以二进制补码表示的整数；
	 * 			最小值是 -128（-2^7）；
	 * 			最大值是 127（2^7-1）；
	 * 			默认值是 0；
	 * 			byte 类型用在大型数组中节约空间，主要代替整数，因为 byte 变量占用的空间只有 int 类型的四分之一；
	 * 			例子：byte a = 100，byte b = -50。
	 * 			Java中最小的数据类型，在内存中占8位(bit)，即1个字节，取值范围-128~127，默认值0
	 * 		short
	 * 			短整型，在内存中占16位，即2个字节，取值范围-32768~32717，默认值0
	 * 			Short 数据类型也可以像 byte 那样节省空间。一个short变量是int型变量所占空间的二分之一；
	 * 		int
	 * 			整型，用于存储整数，在内在中占32位，即4个字节，取值范围-2147483648~2147483647，默认值0
	 * 		long
	 * 			长整型，在内存中占64位，即8个字节-2^63~2^63-1，默认值0L
	 * 		float
	 * 			浮点型，在内存中占32位，即4个字节，用于存储带小数点的数字（与double的区别在于float类型有效小数点只有6~7位），默认值0
	 * 			float 在储存大型浮点数组的时候可节省内存空间；
	 * 			浮点数不能用来表示精确的值，如货币；
	 * 		double
	 * 			double：双精度浮点型，用于存储带有小数点的数字，在内存中占64位，即8个字节，默认值0
	 * 			double类型同样不能表示精确的值，如货币；
	 * 		char
	 * 			字符型，用于存储单个字符，占16位，即2个字节，取值范围0~65535，默认值为空
	 * 			char类型是一个单一的 16 位 Unicode 字符；
	 * 		自动类型转换
	 * 			byte,short,char —> int —> long —> float —> double 
	 * 3，字符串String
	 * 		String字符串常量和String对象的区别
	 * 			String a = new String("hello java");
	 * 			String b = "hello java";
	 * 			a != b;
	 * 				一个在堆区，一个在常量池，他们是不同的两个对象！
	 * 			String a = "hello java";
	 * 			String b = "hello" + "java";
	 * 			a = b;
	 * 				JVM在编译期间会自动把字符串常量相加操作计算完毕后再执行比较，所以计算完成后其实c=“helloJava”，因为字符串存在这样的常量，所以直接引用即可，使用的是同一内存地址的数据。
	 * 			String b = "helloJava";
	 *			String d1 = "hello";
	 *			String d2 = "Java";
	 *			String d = d1 + d2;
	 *			b != d;
	 *				字符串变量的连接动作，在编译阶段会被转化成StringBuilder的append操作，变量d最终指向Java堆上新建String对象，变量b指向常量池的”helloJava”字符串，所以 b != d，其实这也就是文章开头讲String不可变的机制,为什么JVM要这样处理？
	 *				这是因为d1和d2在编译阶段最终指向的对象是不可知的，所以不能当做常量来看待。但是如果添加final关键字的话，那么就是相等的了，因为final关键字强制性的使d1和d2无法再变更，所以可以当做常量看待！其他不等的情况同理
	 * 			String a = new String("helloJava");
     *			String a1 = a.intern();
     *			System.out.println(a == a1);//false
     *			String b = new String("String")+new String("Tests");
     *			String b1 = b.intern();
     *			System.out.println(b == b1);//true
     *			String c = "helloWorld";
     *			String c1 = c.intern();
     *			System.out.println(c == c1);//true
     *				intern
     *					首先查看常量池是否有字符串，如果有直接返回字符串的引用；否则在常量池添加该字符串在堆区的引用，并返回该引用； 
     *				对于第一个例子，执行第一句后，a指向是堆区字符串的引用，同时在常量池也创建了一份字符串，所以a1指向常量池字符串引用，所以引用地址不同自然不等； 
	 * 				对于第二个例子，只在堆区创建了字符串对象，常量池没有！所以执行intern()后实际在常量池中创建了一份该字符串在堆区的引用，b1实际指向的还是堆区的引用，所以相等！
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	
	public static void main(String[] args) {
		byte a = 100;
		short b = -50;
		char c = 1000;
		System.err.println(a + b + c);
	}
}
