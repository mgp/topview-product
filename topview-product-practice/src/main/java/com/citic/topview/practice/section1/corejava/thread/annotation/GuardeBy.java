package com.citic.topview.practice.section1.corejava.thread.annotation;

public @interface GuardeBy {

	String value();

}
