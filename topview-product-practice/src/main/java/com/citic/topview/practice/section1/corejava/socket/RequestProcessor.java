package com.citic.topview.practice.section1.corejava.socket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RequestProcessor{
	
	private static ExecutorService  executorService  = Executors.newFixedThreadPool(10);
	
	public static void processorRequest(final SelectionKey key, final Selector selector){
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				try {
					SocketChannel readChannel = (SocketChannel) key.channel();
					
					ByteBuffer buffer = ByteBuffer.allocate(1024);
	                ByteArrayOutputStream baos = new ByteArrayOutputStream();
	                int len = 0;
	                while (true) {
	                	buffer.clear();
	                    len = readChannel.read(buffer);
	                    
	                    if (len == -1) break;
	                    
	                    buffer.flip();
                        while (buffer.hasRemaining()) {
                            baos.write(buffer.get());
                        }
                        
                        System.out.println("服务器端接收到的数据：" + new String(baos.toByteArray()));
                        
                        key.attach(baos);										// 将数据添加到key中
                        NIOServerSocket.addWriteQueen(key);						// 将读操作的channel变更为写操作
	                }
				} catch (IOException e) {

				}
			}
		});
	}
}


















