package com.citic.topview.practice.section1.corejava.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Preloader {

	private final FutureTask<ProductInfo> futureTask = new FutureTask<>(new Callable<ProductInfo>() {
		public ProductInfo call() throws Exception {
			return loadProductInfo();
		};
	});

	private final Thread thread = new Thread(futureTask);
	
	public void start() {
		thread.start();
	}
	
	public ProductInfo get() throws Throwable{
		try {
			return futureTask.get();
		} catch (ExecutionException e) {
			Throwable cause = e.getCause();
			throw cause;
		}
	}
	
	protected ProductInfo loadProductInfo() {
		return null;
	}
}
