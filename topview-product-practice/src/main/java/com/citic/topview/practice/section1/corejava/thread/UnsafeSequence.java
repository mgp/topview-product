package com.citic.topview.practice.section1.corejava.thread;

import com.citic.topview.practice.section1.corejava.thread.annotation.NotThreadSafe;

@NotThreadSafe
public class UnsafeSequence {

	private int value;
	
	public int getNext() {
		return value ++;
	}
}
