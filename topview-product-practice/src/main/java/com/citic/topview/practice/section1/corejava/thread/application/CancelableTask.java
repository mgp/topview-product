package com.citic.topview.practice.section1.corejava.thread.application;

import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;

public interface CancelableTask <T> extends Callable<T> {

	void cancel();
	
	RunnableFuture<T> newTask();
}
