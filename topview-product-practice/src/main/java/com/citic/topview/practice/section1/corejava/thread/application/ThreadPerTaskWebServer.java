package com.citic.topview.practice.section1.corejava.thread.application;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ThreadPerTaskWebServer {

	private static ServerSocket serverSocket;

	public static void main(String[] args) throws IOException {
		serverSocket = new ServerSocket(8088);
		while(true) {
			final Socket socket = serverSocket.accept();
			Runnable task = new Runnable() {
				@Override
				public void run() {
					handleRequest(socket);
				}
			};
			
			new Thread(task).start();
		}
	}

	protected static void handleRequest(Socket socket) {
		
	}
}
