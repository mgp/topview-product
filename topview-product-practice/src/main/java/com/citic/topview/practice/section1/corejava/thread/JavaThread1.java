package com.citic.topview.practice.section1.corejava.thread;

public class JavaThread1 {

	/**
	 * 知识点
	 * https://blog.csdn.net/ahaha413525642/article/category/7204691
	 * https://www.jianshu.com/nb/16738693
	 * 	为了安全地发布对象，对象的引用以及对象的状态必须同时对其他线程可见。一个正确创建的对象可以通过下列条件安全的发布：
	 * 		通过静态初始化器初始化对象的应用
	 * 		将他的引用存储到volatile域或AtomicReference域中
	 * 		将他的引用存储到正确创建的对象的final域中
	 * 		将他的引用存储到由锁正确保护的域中
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
	}
}
