package com.citic.topview.practice.section1.corejava.thread;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class VolatileCachedFactors implements Servlet{

	private volatile OneValueCache cache = new OneValueCache(null, null);
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		
	}

	@Override
	public ServletConfig getServletConfig() {
		return null;
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		BigInteger integer = BigInteger.valueOf(Long.valueOf(req.getParameter("")));
		BigInteger[] factors = cache.getFactors(integer);
		
		if(factors == null){
			factors = null;
			cache = new OneValueCache(integer, factors);
		}
	}

	@Override
	public String getServletInfo() {
		return null;
	}

	@Override
	public void destroy() {
		
	}
}
