package com.citic.topview.practice.section1.corejava.thread.application;

import java.io.PrintWriter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class LogWriter2 {

	private final BlockingQueue<String> queue;
	private final LoggerThread logger;
	private final PrintWriter writer;
	
	private boolean isShutdown = false;
	private int reservations = 0;
	
	public LogWriter2(PrintWriter writer) {
		this.queue = new LinkedBlockingQueue<String>();
		this.writer = writer;
		this.logger = new LoggerThread();
	}
	
	public void start() {
		logger.start();
	}
	
	public void stop() {
		synchronized (this) {
			isShutdown = true;
		}
		logger.interrupt();
	}
	
	public void log(String msg) throws InterruptedException {
		synchronized (this) {
			if(isShutdown)
				throw new IllegalStateException();
			++ reservations;
		}
		
		queue.put(msg);
	}
	
	class LoggerThread extends Thread{
		@Override
		public void run() {
			try {
				while (true) {
					try {
						synchronized (LogWriter2.this) {
							if(isShutdown && reservations == 0) {
								break;
							}
						}
						
						String msg = queue.take();
						synchronized (LogWriter2.this) {
							-- reservations;
						}
						writer.println(msg);
					} catch (Exception e) {}
				}
			} finally {
				writer.close();
			}
		}
	}
}
