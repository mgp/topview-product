package com.citic.topview.practice.section1.corejava.thread.application;

import java.math.BigInteger;
import java.util.concurrent.BlockingQueue;

public class PrimeProducer extends Thread{

	private final BlockingQueue<BigInteger> queue;
	
	public PrimeProducer(BlockingQueue<BigInteger> queue) {
		this.queue = queue;
	}
	
	@Override
	public void run() {
		BigInteger bigInteger = BigInteger.ONE;
		while (!Thread.currentThread().isInterrupted()) {
			try {
				queue.put(bigInteger = bigInteger.nextProbablePrime());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void interrupt() {
		interrupt();
	}
}
