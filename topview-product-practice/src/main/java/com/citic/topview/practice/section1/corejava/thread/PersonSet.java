package com.citic.topview.practice.section1.corejava.thread;

import java.util.HashSet;
import java.util.Set;

import com.citic.topview.practice.section1.corejava.thread.annotation.ThreadSafe;

/**
 * 实例限制是构建线程安全类的最简单的方法之一
 * @author Administrator
 *
 */
@ThreadSafe
public class PersonSet {

	private final Set<Person> mySet = new HashSet<>();

	public synchronized void addPerson(Person person) {
		mySet.add(person);
	}
	
	public synchronized boolean containPerson(Person person) {
		return mySet.contains(person);
	}
}
