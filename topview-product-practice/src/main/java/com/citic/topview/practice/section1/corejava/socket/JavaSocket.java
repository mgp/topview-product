package com.citic.topview.practice.section1.corejava.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class JavaSocket {

	/**
	 * 网络概述
	 * 	计算机网络概述
	 * 	网络编程概述
	 * 网络编程技术
	 * 	网络编程步骤
	 * 		客户端网络编程步骤
	 * 			建立网络连接
	 * 			交换数据
	 * 			关闭网络连接
	 * 		服务器端网络编程步骤
	 * 			监听端口
	 * 			获得连接
	 * 			交换数据
	 * 			关闭连接
	 * Java网络编程技术
	 * 	InetAddress
	 * 		基础的网络类——InetAddress类。该类的功能是代表一个IP地址，并且将IP地址和域名相关的操作方法包含在该类的内部。
	 * 
	 * @param args
	 */
	public static void mains(String[] args) {
		
	}
	
	public void test() throws UnknownHostException {
		InetAddress inet1 = InetAddress.getByName("www.163.com"); 				// 使用域名创建对象
		System.out.println(inet1);
		InetAddress inet2 = InetAddress.getByName("127.0.0.1");					// 使用IP创建对象
		System.out.println(inet2);
        InetAddress inet3 = InetAddress.getLocalHost();							// 获得本机地址对象
	
        String host = inet3.getHostName();										// 获得对象中存储的域名
        System.out.println("域名：" + host);

        String ip = inet3.getHostAddress();										// 获得对象中存储的IP
        System.out.println("IP:" + ip);

	}
	
	public void client() throws UnknownHostException, IOException {
		Socket socket = new Socket("127.0.0.1", 8888);
		OutputStream os = socket.getOutputStream();
		os.write("hello".getBytes());
		
		InputStream is = socket.getInputStream();
		byte[] b = new byte[1024];
        int n = is.read(b);
        System.out.println("服务器反馈：" + new String(b,0,n));
		
		socket.close();
	}
	
	public void udpSend() throws IOException {
		DatagramSocket ds = new DatagramSocket(8888);
		 
		byte[] b = "hello".getBytes();
		InetAddress server = InetAddress.getByName("127.0.0.1");
		DatagramPacket sendUDP = new DatagramPacket(b, b.length, server, 8888);
		ds.send(sendUDP);
		
		ds.close();
	}
	
	public void udpReceive() throws IOException {
		DatagramSocket ds = new DatagramSocket(8888);
		
		byte[] data = new byte[1024];
        DatagramPacket receiveDp = new DatagramPacket(data, data.length);
        ds.receive(receiveDp);
        byte[] b = receiveDp.getData(); //获得缓冲数组
        int len = receiveDp.getLength(); //获得有效数据长度
        String s = new String(b, 0, len);
        System.out.println(s);
        
		ds.close();
	}
	
	public void nioSocket() throws IOException {
		Selector selector = Selector.open();
		ServerSocketChannel servChannel = ServerSocketChannel.open();
		servChannel.configureBlocking(false);
		servChannel.socket().bind(new InetSocketAddress(8080), 1024);				// 绑定端口号
        servChannel.register(selector, SelectionKey.OP_ACCEPT);						// 注册感兴趣事件
        
        selector.select(1000);														// select系统调用
        
        Set<SelectionKey> selectedKeys = selector.selectedKeys();
        Iterator<SelectionKey> it = selectedKeys.iterator();
        SelectionKey key = null;
        while (it.hasNext()) {
        	key = it.next();
            it.remove();
            if (key.isValid()) {
            	if (key.isAcceptable()) {
            		ServerSocketChannel ssc = (ServerSocketChannel) key.channel();	// 接收客户端的连接，并创建一个SocketChannel
                    SocketChannel sc = ssc.accept(); 
                    sc.configureBlocking(false);
                    sc.register(selector, SelectionKey.OP_READ); 					// 将SocketChannel和感兴趣事件注册到selector
            	}
            	
            	if (key.isReadable()) {
            		// 读数据的处理
            	}
            }
		}
	}
	
}



