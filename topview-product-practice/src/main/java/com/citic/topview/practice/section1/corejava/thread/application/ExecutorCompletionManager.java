package com.citic.topview.practice.section1.corejava.thread.application;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorCompletionManager {
	
	static ExecutorCompletionService<String> service = new ExecutorCompletionService<String>(Executors.newCachedThreadPool());
	
	public static void main(String[] args) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < 10; i++) {
					int index = i;
					service.submit(new Callable<String>() {
						@Override
						public String call() throws Exception {
							return "task done" + index;
						}
					});
				}				
			}
		}).start();
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						Future<String> take = service.take(); // take方法是阻塞的
						take.get();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (ExecutionException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
}
