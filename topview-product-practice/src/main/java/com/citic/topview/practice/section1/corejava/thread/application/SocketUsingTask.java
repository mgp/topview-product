package com.citic.topview.practice.section1.corejava.thread.application;

import java.net.Socket;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

public abstract class SocketUsingTask<T> implements CancelableTask<T>{

	private Socket socket;
	
	public synchronized void setSocket(Socket socket) {
		this.socket = socket;
	}
	
	@Override
	public synchronized void cancel() {
		try {
			if(socket != null)
				socket.close();
		} catch (Exception e) {
		}
	}
	
	public RunnableFuture<T> newTask(){
		return new FutureTask<T>(this) {
			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				SocketUsingTask.this.cancel();
				return super.cancel(mayInterruptIfRunning);
			}
		};
	}
}
