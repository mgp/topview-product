package com.citic.topview.practice.section1.corejava.socket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ResponeProcessor {

	private static ExecutorService  executorService  = Executors.newFixedThreadPool(10);
	
	public static void processorRespone(final SelectionKey key) {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				try {
                    SocketChannel writeChannel = (SocketChannel) key.channel();						// 写操作
                    ByteArrayOutputStream attachment = (ByteArrayOutputStream)key.attachment();		// 拿到客户端传递的数据

                    System.out.println("客户端发送来的数据：" + new String(attachment.toByteArray()));

                    ByteBuffer buffer = ByteBuffer.allocate(1024);
                    String message = "你好，我好，大家好！！";
                    buffer.put(message.getBytes());
                    buffer.flip();
                    
                    writeChannel.write(buffer);
                    writeChannel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }  
			}
		});
	}
}
