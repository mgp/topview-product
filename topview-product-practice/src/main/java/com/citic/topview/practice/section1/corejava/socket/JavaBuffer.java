package com.citic.topview.practice.section1.corejava.socket;

import java.nio.CharBuffer;

public class JavaBuffer {

	/**
	 * Buffer
	 * 		每种类型都有对应的Buffer，但是常用的Buffer为ByteBuffer和CharBuffer。Buffer具体的实例由XXXBuffer.allocate(int capacity)来获取。
	 * 		ByteBuffer还有一个子类MappedByteBuffer，这个子类通常通过Channel的map()方法返回，该缓冲区将磁盘的全部或部分内容映射到内存(虚拟内存)。读写性能较高。
	 * 	容量（capacity）：缓冲区能够容纳的数据元素的最大数量。这一个容量在缓冲区创建时被设定，并且永远不能改变。
	 * 	上界(limit)：缓冲区的第一个不能被读或写的元素。或者说,缓冲区中现存元素的计数。
	 * 	位置(position)：下一个要被读或写的元素的索引。位置会自动由相应的 get()和 put()函数更新。
	 * 	标记(mark)：下一个要被读或写的元素的索引。位置会自动由相应的 get()和 put()函数更新。
	 * 		其中 0 <= mark <= position <= limit <= capacity
	 * 
	 * 	flip() {翻动}: limit设为position的位置，position设为0，读写指针又移动到了开始的位置，调用flip()后，buffer为输出数据做好了准备。
	 * 	clear() ：position 变成 0 ， limit 变成 capacity (清空 buffer ，准备再次被写入) 。
	 * 	compact(): 将未读取的数据拷贝到 buffer 的头部位。
	 * 
	 * Selector
	 * 	选择器属性：
	 *		选择器（Selector）
     *			选择器类管理着一个被注册的通道集合的信息和它们的就绪状态。通道是和选择器一起被注册的,并且使用选择器来更新通道的就绪状态。当这么做的时候,可以选择将被激发的线程挂起,直到有就绪的的通道。
	 *		可选择通道(SelectableChannel)
     *			SelectableChannel 可以被注册到 Selector 对象上,同时可以指定对那个选择器而言,那种操作是感兴趣的。一个通道可以被注册到多个选择器上,但对每个选择器而言只能被注册一次。
	 *		选择键(SelectionKey)
     *			选择键封装了特定的通道与特定的选择器的注册关系。选择键对象被SelectableChannel.register( ) 返回并提供一个表示这种注册关系的标记。选择键包含了两个比特集(以整数的形式进行编码),指示了该注册关系所关心的通道操作,以及通道已经准备好的操作。
	 *	选择器使用：
	 *		创建：使用Selector selector = Selector.open()
	 *		将channel注册到selector：channel.register(selector, **)，该方法可返回一个SelectorKey，代表该channel在selector中的”代号”。与selector一起使用时，channel一定处于非阻塞模式下。所以需要提前设置channel.configureBlocking(false)。
	 *		使用selector几个select的重载方法测试通道有多少已经准备好了： 
	 *			int select()阻塞到至少有一个对象在通道上准备好了。
	 *			int select(long timeout)最长会阻塞timeout毫秒
	 *			int selectNow()不会阻塞，不管什么情况会直接返回，如果自上次调用选择操作以来，没有就绪的通道则直接返回0
	 *			调用select方法之后，如果有返回值，则说明已经有selector就绪了。此时可以通过selectedKey来选择已选择键集中就绪的通道。使用Set<SelectionKey> selectedKey = selector.selectedKeys();，之后在得到的SelectionKey的Set中可以通过SelectionKey提供的方法来操作channel。得到Channel的基本方法为selectionKey.channel() 。
	 *
	 *
	 *http://ifeve.com/java-nio-vs-io/
	 *
	 *
	 *
	 * 	
	 * @param args
	 */
	public static void main(String[] args) {
		CharBuffer buff = CharBuffer.allocate(8);
        
        // add
        buff.put('a');
        buff.put('b');
        buff.put('c');
        System.out.println("limit: " + buff.limit());
        System.out.println("position: " + buff.position());
        
        // flip
        buff.flip();
        System.out.println("limit: " + buff.limit());
        System.out.println("position: " + buff.position());
        
        // clear
        buff.clear();
        System.out.println("limit: " + buff.limit());
        System.out.println("position: " + buff.position());
        
        // compact
        buff.compact();
        System.out.println("limit: " + buff.limit());
        System.out.println("position: " + buff.position());
	}
}
