package com.citic.topview.practice.redis;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citic.topview.practice.utils.RedisUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestRedis {

	@Autowired
	private RedisUtil redisUtil;
	
	@Test
	public void testRedis() {
		Object username = "topview";
		redisUtil.set("username", username, 2);
		Object un = redisUtil.get("username");
		System.err.println(un);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.err.println(redisUtil.get("username"));
		
		redisUtil.expire("username", 2);
		
		System.err.println(redisUtil.get("username"));
	}
}
