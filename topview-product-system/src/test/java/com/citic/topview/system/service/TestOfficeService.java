package com.citic.topview.system.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.citic.topview.TopviewProductSystemApplication;
import com.citic.topview.common.system.entity.Office;
import com.citic.topview.common.system.vo.OfficeVO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={TopviewProductSystemApplication.class, TestOfficeService.class})
public class TestOfficeService {

	@Autowired
	OfficeService officeService;
	
	@Test
	public void testSave() {
		Office office = new Office();
		office.setParentId(0L);
		office.setName("中国中信集团有限公司");
		office.setSort(10);
		office.setCode("100000");
		office.setType("1");
		office.setGrade("1");
		office.setAddress("京城大厦");
		office.setEmail("zhongxin@citic.com");
		office.setPhone("110");
		office.setUseable("0");
		
		System.err.println(officeService.save(office));
	}
	
	@Test
	public void testList() {
		Map<String, Object> params = new HashMap<>();
		List<OfficeVO> list = officeService.list(params);
		
		for (OfficeVO officeVO : list) {
			System.err.println(officeVO);
		}
	}
}
