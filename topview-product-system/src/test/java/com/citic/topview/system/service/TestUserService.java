package com.citic.topview.system.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.citic.topview.TopviewProductSystemApplication;
import com.citic.topview.common.system.entity.User;
import com.citic.topview.common.system.vo.UserVO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={TopviewProductSystemApplication.class, TestUserService.class})
public class TestUserService {

	@Autowired
	UserService userService;
	
	@Test
	public void testSave() {
		User user = new User();
		user.setCompanyId(273031075087777792L);
		user.setLoginName("topview");
		user.setName("topview");
		user.setNo("topview100000000");
		user.setCode("topview100000000");
		user.setEmail("topview@citic.com");
		user.setMobile("18738102014");
		user.setUserType("0");
		user.setGender("0");
		user.setStatus("0");
		user.setPassword(SystemService.entryptPassword("880204"));
		
		System.err.println(userService.save(user));
	}
	
	@Test
	public void testList() {
		Map<String, Object> params = new HashMap<>();
		List<UserVO> list = userService.list(params);
		
		for (UserVO userVO : list) {
			System.err.println(userVO);
		}
	}
}
