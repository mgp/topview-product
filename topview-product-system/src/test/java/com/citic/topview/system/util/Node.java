package com.citic.topview.system.util;

import java.util.List;

public class Node {

	private Long id;
	private Long parentId;
	private String name;
	private List<Node> children;
	
	public Node() {}
	
	public Node(Long id, Long parentId, String name){
		this.id = id;
		this.parentId = parentId;
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Node> getChildren() {
		return children;
	}
	public void setChildren(List<Node> children) {
		this.children = children;
	}
}
