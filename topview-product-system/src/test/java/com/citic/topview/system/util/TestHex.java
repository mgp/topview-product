package com.citic.topview.system.util;

import java.io.UnsupportedEncodingException;

import org.junit.Test;

import com.citic.topview.common.util.EncodesUtil;
import com.citic.topview.system.security.manager.Digests;

public class TestHex {

	@Test
	public void test() throws UnsupportedEncodingException {
		String plain = "880204";
				
		byte[] salt = new byte[8];
		salt[0] = 1;
		salt[1] = 2;
		salt[2] = 3;
		salt[3] = 4;
		salt[4] = 5;
		salt[5] = 6;
		salt[6] = 7;
		salt[7] = 8;
		
		byte[] hashPassword = Digests.sha1(plain.getBytes(), salt, 512);
		System.err.println(hashPassword);
		String string = EncodesUtil.encodeHex(hashPassword);
		System.err.println(string);
		
		System.err.println(EncodesUtil.encodeHex(Digests.generateSalt(8)));
	}
	
	@Test
	public void entryptPasswordByApp() {
		String plainPassword = "880204";
		String plain = EncodesUtil.unescapeHtml(plainPassword);
		byte[] salt = Digests.generateSalt(8);
		byte[] hashPassword = Digests.sha1(plain.getBytes(), salt, 512);
		System.err.println(EncodesUtil.encodeHex(salt) + EncodesUtil.encodeHex(hashPassword));;
	}
	
	@Test
	public void decode() {
		String accountCredentials = "8c28f0235b1d5d21330f842edc7ce7d896c9042e10db00726f0646ee";
		byte[] salt = EncodesUtil.decodeHex(accountCredentials.substring(0, 16));
		String accountPassword = accountCredentials.substring(16);
		
		String tokenCredentials = "880204";
		byte[] tokenHashPassword = Digests.sha1(tokenCredentials.getBytes(), salt, 512);
		tokenCredentials = EncodesUtil.encodeHex(tokenHashPassword);
		
        System.err.println(accountPassword);
        System.err.println(tokenCredentials);
	}
}
