
package com.citic.topview.system.util;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class TestCache {

	@Test
	public void test() {
		Cache<String, String> cache = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.SECONDS).build();
        cache.put("username", "topview");
        System.out.println(cache.getIfPresent("username"));
        
        try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        
        System.out.println(cache.getIfPresent("username"));
	}
}
