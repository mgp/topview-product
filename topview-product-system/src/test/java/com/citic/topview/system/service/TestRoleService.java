package com.citic.topview.system.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.citic.topview.TopviewProductSystemApplication;
import com.citic.topview.common.system.entity.Role;
import com.citic.topview.common.system.entity.Role.RoleBuilder;
import com.citic.topview.common.system.vo.RoleVO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={TopviewProductSystemApplication.class, TestRoleService.class})
public class TestRoleService {

	@Autowired
	RoleService roleService;
	
	@Test
	public void testSave() {
		Role role = new RoleBuilder().withName("系统管理员").withEnname("systemAdmin").build();
		role.setIsSys("1");
		role.setRoleType("assignment");
		role.setUseable("1");
		role.setDataScope("1");
		
		System.err.println(roleService.save(role));
	}
	
	@Test
	public void testList() {
		Map<String, Object> params = new HashMap<>();
		List<RoleVO> list = roleService.list(params);
		
		for (RoleVO roleVO : list) {
			System.err.println(roleVO);
		}
	}
}
