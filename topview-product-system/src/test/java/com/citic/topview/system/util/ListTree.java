package com.citic.topview.system.util;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ListTree {

	@Test
	public void test() {
		List<Node> list = getList();
		for (Node Node : list) {
			initdata(Node, list);
		}
		
		System.err.println(list.size());
	}

	private void initdata(Node node, List<Node> list) {
		List<Node> children = new ArrayList<>();
		for (Node vo : list) {
			if(node.getId().equals(vo.getParentId())) {
				initdata(vo, list);
				children.add(vo);
			}
		}
		node.setChildren(children);
	}
	
	private List<Node> getList() {
		List<Node> list = new ArrayList<>();
		
		Node node1 = new Node(1L, 0L, "1");
		Node node11 = new Node(11L, 1L, "11");
		Node node111 = new Node(111L, 11L, "111");
		Node node112 = new Node(112L, 11L, "112");
		Node node113 = new Node(113L, 11L, "113");
		Node node12 = new Node(12L, 1L, "12");
		Node node13 = new Node(13L, 1L, "13");
		
		Node node2 = new Node(2L, 0L, "2");
		Node node21 = new Node(21L, 2L, "21");
		Node node22 = new Node(22L, 2L, "22");
		Node node23 = new Node(23L, 2L, "23");
		
		Node node3 = new Node(3L, 0L, "3");
		Node node31 = new Node(31L, 3L, "31");
		Node node32 = new Node(32L, 3L, "32");
		Node node33 = new Node(33L, 3L, "33");
		
		Node node4 = new Node(4L, 0L, "4");
		Node node41 = new Node(41L, 4L, "41");
		Node node42 = new Node(42L, 4L, "42");
		Node node43 = new Node(43L, 4L, "43");
		Node node431 = new Node(431L, 43L, "431");
		Node node432 = new Node(432L, 43L, "432");
		Node node433 = new Node(433L, 43L, "433");
		
		Node node4331 = new Node(4331L, 433L, "4331");
		Node node4332 = new Node(4332L, 433L, "4332");
		Node node4333 = new Node(4333L, 433L, "4333");
		
		Node node5 = new Node(5L, 0L, "5");
		
		
		list.add(node1);
		list.add(node11);
		list.add(node111);
		list.add(node112);
		list.add(node113);
		list.add(node12);
		list.add(node13);
		
		list.add(node2);
		list.add(node21);
		list.add(node22);
		list.add(node23);
		
		list.add(node3);
		list.add(node31);
		list.add(node32);
		list.add(node33);
		
		list.add(node4);
		list.add(node41);
		list.add(node42);
		list.add(node43);
		list.add(node431);
		list.add(node432);
		list.add(node433);
		list.add(node4331);
		list.add(node4332);
		list.add(node4333);
		
		list.add(node5);
		
		return list;
	}
}
