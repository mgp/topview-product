$(document).ready(function () {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules : {
            name : {required : true,},
            password : {required : true,},
            officeName : {required : true,},
        },
        messages : {
            name : {required : icon + "请输入名称"},
            password : {required : icon + "请输入密码"},
            officeName : {required : icon + "请输入机构"},
        }
    });
});
$.validator.setDefaults({
	submitHandler : function() {
		var ids = "";
		$("input:checkbox[name=role]:checked").each(function(i) {
			if (0 == i) {
				ids = $(this).val();
			} else {
				ids += ("," + $(this).val());
			}
		});
		$("#roleIds").val(ids);
		
        $.ajax({
            cache : false,
            type : "POST",
            url : ctx + "a/appUser/save",
            data : $('#signupForm').serialize(),// 你的formid
            async : false,
            error : function(request) {parent.layer.alert("Connection error", {offset: "100px"});},
            success : function(data) {
                if (data.result == "success") {
                    parent.layer.msg("操作成功");
                    parent.reLoad();
                    var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                    parent.layer.close(index);
                } else {
                    parent.layer.alert("操作失败", {offset: "100px"})
                }
            }
        });
	}
});

function openOffice(){
	layer.open({
		type:2,
		title:"选择机构",
		area : [ '300px', '450px' ],
		content: ctx + "a/office/tree",
	})
}

function loadOffice(officeId, officeName){
	$("#officeId").val(officeId);
	$("#officeName").val(officeName);
}