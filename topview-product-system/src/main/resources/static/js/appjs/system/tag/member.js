var memberUserIds = new Set();
var memberOfficeIds = new Set();
var hasMember = new Set();
$(document).ready(function () {
	$.ajax({
		type : "GET",
		url : ctx + 'a/tag/listIds',
		data: {id: parent.selectTagId},
		success : function(result) {
			var list = result.bizVO;
			if(list.length > 0){
				for(var i = 0; i < list.length; i ++){
					if(list[i].memberType == '0'){
						memberUserIds.add(list[i].memberId);
					} else {
						memberOfficeIds.add(list[i].memberId);
					}
					hasMember.add(list[i].memberId);
					createElement(list[i].memberId, list[i].name, list[i].memberType);
				}
			}
			
			getListAll("");
		}
	});
	
	$.ajax({
		type : "POST",
		url : ctx + 'a/office/tree',
		success : function(tree) {
			$('#jstree1').jstree({'core' : {'data' : tree},"plugins" : [ "search" ]});
			$('#jstree2').jstree({'core' : {'data' : tree},"plugins" : [ "search" ]});
		}
	});
	
	/*********************************************************************************/
	$("#jstree1").on("loaded.jstree", function (event, data) {
        $('#jstree1').jstree('close_all');
    });
	
	$('#jstree1').on("changed.jstree", function(e, data) {
		var id = data.node.id;
		var name = data.node.text;
		if(!memberOfficeIds.has(id)){
			memberOfficeIds.add(id);
			hasMember.add(id);
			createElement(id, name, "1");
		}
	});	
	
	/*********************************************************************************/
	$("#jstree2").on("loaded.jstree", function (event, data) {
		$('#jstree2').jstree('open_all');
	});
	
	$('#jstree2').on("changed.jstree", function(e, data) {
		getListAll(data.node.id);
	});	
});

function createElement(id, name, type){
	var img = "search-dep.png";
	if(type == "0") img = "default_user.png";
	
	var html = 	"<div class='tag-member-item'>" +
					"<span class='tag-member-item-left'>" + 
						"<span class='tab-member-item-head'>" + 
							"<img class='tag-member-item-img' src='/topview/img/" + img + "'/>" + 
							"<span class='tag-member-item-name'>" + name + "</span>" + 
						"</span>" + 
						"<span class='tab-member-item-nbsp'>&nbsp;</span>" + 
					"</span>" + 
					"<span class='tag-member-item-x' memberId='"+id+"' memberName='"+name+"' onclick='removeMember(this);'>×</span>" + 
				"</div>";
	$(".tag-member").append(html);
}

function getHasMember(){
	
}

function getListAll(officeId){
	$.ajax({
		type : "GET",
		url : ctx + 'a/appUser/listAll',
        data: {'officeId': officeId, 'userType': '0'},
		success : function(list) {
			$(".member-right").children().remove();
			for(var i = 0; i < list.length; i ++){
				var bg = "transparent";
				var className = "";
				
				if(hasMember.has(list[i].id)){
					bg = "rgb(236, 236, 236)";
					className = "ant-checkbox-checked";
				}
				
				var html = 	"<div class='icon_box_content'>" +
							"<div class='box_box icon_box' style='background-color: " + bg + ";' onclick='addMember(this, "+ '"' + list[i].id + '"' + ", " + '"' + list[i].name + '"' + ");'>" +
								"<img src='/topview/img/default_user.png' class='box_vam icon_user' style='width: 28px; height: 28px; margin-left: 2px;'>" + 
								"<span class='box_vam icon_name' id='" + list[i].id + "'>" + list[i].name + "</span>" + 
								"<label class='box_vam icon_label ant-checkbox-wrapper'>" + 
									"<span class='ant-checkbox " + className + "'>" + 
										"<span class='ant-checkbox-inner'></span>" + 
										"<input type='checkbox' class='ant-checkbox-input' value='on'>" + 
									"</span>" + 
								"</label>" + 
							"</div>" + 
							"<div style='height: 1px; background-color: rgb(229, 229, 229);'></div>" + 
							"</div>";
				$(".member-right").append(html);
			}
		}
	});
}
function addMember(obj, id, name){
	if(!memberUserIds.has(id)){
		memberUserIds.add(id);
		hasMember.add(id);
		
		$(obj).css("background-color", "rgb(236, 236, 236)");
		$(obj).find(".ant-checkbox").addClass("ant-checkbox-checked");
		
		createElement(id, name, "0");
	}
}
function removeMember(obj){
	var id = $(obj).attr("memberId");
	hasMember.delete(id);
	memberUserIds.delete(id);
	memberOfficeIds.delete(id);
	
	$(".icon_box_content .icon_name").each(function(index, item){
		if($(this).attr("id") == id){
			$(this).next().find(".ant-checkbox").removeClass("ant-checkbox-checked");
			$(this).parent().css("background-color", "transparent");
		}
	});
	
	$(obj).parent().remove();
}
function getMember(){
	var array = new Array();
	array.push(memberUserIds);
	array.push(memberOfficeIds);
	return array;
}