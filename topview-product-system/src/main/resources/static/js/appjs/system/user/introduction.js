$(document).ready(function () {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules : {
            password : {required : true,},
            newPassword : {required : true,},
            confirmPassword : {required : true, equalTo: "#newPassword"},
        },
        messages : {
            password : {required : icon + "请输入旧密码"},
            newPassword : {required : icon + "请输入新密码"},
            confirmPassword : {required : icon + "请输入确认密码", equalTo: icon + "确认密码与新密码不同"},
        }
    });
});
$.validator.setDefaults({
	submitHandler : function() {
        $.ajax({
            cache : false,
            type : "POST",
            url : ctx + "a/user/updatePassword",
            data : $('#signupForm').serialize(),// 你的formid
            async : false,
            error : function(request) {parent.layer.alert("Connection error", {offset: "100px"});},
            success : function(data) {
            	parent.layer.msg(data.message);
            }
        });
	}
});
