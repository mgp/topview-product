
$(function() {
	$(document).ready(function() {
		$(".sidebar-toggle").click(function() {
			if($(".sidebar-mini").hasClass("sidebar-collapse")){
				$(".sidebar-mini").removeClass("sidebar-collapse");
			} else {
				$(".sidebar-mini").addClass("sidebar-collapse");
			}
		});
		
        $("#side-menu>li").hover(function(){
            if($("body.skin-blue").hasClass("sidebar-collapse")){
                $(this).find("ul").attr("style", "");
                $(this).find("ul").addClass("hover-class");
            }
        },function(){
            if($("body.skin-blue").hasClass("sidebar-collapse")){
                $(this).find("ul").attr("style", "");
                $(this).find("ul").removeClass("hover-class");
            }
        });
	});
});