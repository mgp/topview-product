package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.system.entity.TagUser;
import com.citic.topview.common.system.vo.TagUserVO;
import com.citic.topview.common.system.vo.TagVO;

@MyBatisDao
public interface TagUserDao {
	
	public TagUser get(Long id);
	
	public int save(TagUser entity);
	
	public int batchSave(List<TagUser> list);
	
	public int update(TagUser entity);
	
	public int remove(Long id);
	
	public int removeByTagId(Long tagId);
	
	public List<TagUserVO> list(Map<String, Object> params);

	public int removeByVO(TagVO tagVO);

}