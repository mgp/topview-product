package com.citic.topview.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.bean.Tree;
import com.citic.topview.common.system.entity.Menu;
import com.citic.topview.common.system.vo.MenuVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.system.service.MenuService;

@Controller
@RequestMapping("/a/menu")
public class MenuController {

	@Autowired
	private MenuService menuService;
	
	@ModelAttribute("menu")
	public Menu get(@RequestParam(required=false) Long id) {
		Menu entity = null;
		if (id != null && id > 0L){
			entity = menuService.get(id);
		}
		if (entity == null){
			entity = new Menu();
		}
		return entity;
	}
	
	@RequiresPermissions("menu:list")
	@RequestMapping("")
	public String menu(Model model) {
		return "system/menu/list";
	}
	
	@Log("菜单列表")
	@RequiresPermissions("menu:list")
	@RequestMapping("/list")
	@ResponseBody
	public List<MenuVO> list(@RequestParam Map<String, Object> params) {
		List<MenuVO> menus = menuService.listQuery(params);
		return menus;
	}
	
	@Log("菜单添加")
	@RequiresPermissions("menu:edit")
	@GetMapping("/add/{parentId}")
	public String add(Model model, @PathVariable("parentId") Long parentId) {
		model.addAttribute("parentId", parentId);
		if (parentId == 0) {
			model.addAttribute("parentName", "根目录");
		} else {
			model.addAttribute("parentName", menuService.get(parentId).getName());
		}
		
		return "system/menu/add";
	}
	
	@Log("菜单编辑")
	@RequiresPermissions("menu:edit")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		Menu menu = menuService.get(id);
		model.addAttribute("parentId", String.valueOf(menu.getParentId()));
		if (menu.getParentId() == 0) {
			model.addAttribute("parentName", "根目录");
		} else {
			model.addAttribute("parentName", menu.getName());
		}
		model.addAttribute("entity", menu);
		
		return "system/menu/edit";
	}
	
	@Log("菜单保存")
	@RequiresPermissions("menu:edit")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("menu") Menu menu) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(menuService.save(menu) > 0) {
			//userRealm.clearAllCache();
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("菜单更新")
	@RequiresPermissions("menu:edit")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("menu") Menu menu) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(menuService.update(menu) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@RequiresPermissions("menu:edit")
	@Log("菜单删除")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(menuService.removes(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@GetMapping("/tree")
	@ResponseBody
	Tree<MenuVO> tree() {
		Tree<MenuVO>  tree = menuService.getTree();
		return tree;
	}

	@GetMapping("/tree/{roleId}")
	@ResponseBody
	Tree<MenuVO> tree(@PathVariable("roleId") Long roleId) {
		Tree<MenuVO> tree = menuService.getTree(roleId);
		return tree;
	}
}
