package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.system.entity.Log;
import com.citic.topview.common.system.vo.LogVO;

@MyBatisDao
public interface LogDao extends CrudDao<Log>{

	List<LogVO> list(Map<String, Object> params);
	
	List<LogVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);
}
