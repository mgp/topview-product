package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.system.entity.AppException;
import com.citic.topview.common.system.vo.AppExceptionVO;

@MyBatisDao
public interface AppExceptionDao extends CrudDao<AppException>{

	List<AppExceptionVO> list(Map<String, Object> params);
	
	List<AppExceptionVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);
}
