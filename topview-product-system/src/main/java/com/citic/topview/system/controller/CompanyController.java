package com.citic.topview.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.bean.Tree;
import com.citic.topview.common.system.entity.Office;
import com.citic.topview.common.system.vo.OfficeVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.system.service.OfficeService;

@Controller
@RequestMapping("/a/company")
public class CompanyController {

	@Autowired
	private OfficeService officeService;
	
	@ModelAttribute("office")
	public Office get(@RequestParam(required=false) Long id) {
		Office entity = null;
		if (id != null && id > 0L){
			entity = officeService.get(id);
		}
		if (entity == null){
			entity = new Office();
		}
		return entity;
	}
	
	@RequiresPermissions("company:list")
	@RequestMapping("")
	public String office(Model model) {
		return "system/company/list";
	}
	
	@Log("公司列表")
	@RequiresPermissions("company:list")
	@RequestMapping("/list")
	@ResponseBody
	public List<OfficeVO> list(@RequestParam Map<String, Object> params) {
		params.put("type", OfficeService.TYPE_01);
		List<OfficeVO> offices = officeService.listQuery(params);
		return offices;
	}
	
	@Log("公司添加")
	@RequiresPermissions("company:edit")
	@GetMapping("/add/{parentId}")
	public String add(Model model, @PathVariable("parentId") Long parentId) {
		model.addAttribute("parentId", parentId);
		if (parentId == 0) {
			model.addAttribute("parentName", "根目录");
		} else {
			model.addAttribute("parentName", officeService.get(parentId).getName());
		}
		
		return "system/company/add";
	}
	
	@Log("公司编辑")
	@RequiresPermissions("company:edit")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		Office office = officeService.get(id);
		model.addAttribute("parentId", office.getParentId());
		if (office.getParentId() == 0) {
			model.addAttribute("parentName", "根目录");
		} else {
			model.addAttribute("parentName", officeService.get(office.getParentId()).getName());
		}
		model.addAttribute("entity", office);
		
		return "system/company/edit";
	}
	
	@Log("公司保存")
	@RequiresPermissions("company:edit")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("office") Office office) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(officeService.save(office) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("公司更新")
	@RequiresPermissions("company:edit")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("office") Office office) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(officeService.update(office) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("公司删除")
	@RequiresPermissions("company:edit")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(officeService.removes(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@GetMapping("/tree")
	String treeView() {
		return  "system/company/tree";
	}
	
	@PostMapping("/tree")
	@ResponseBody
	public Tree<OfficeVO> tree() {
		Tree<OfficeVO> tree = new Tree<OfficeVO>();
		tree = officeService.getCompanyTree();
		return tree;
	}
}
