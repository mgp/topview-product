package com.citic.topview.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.system.entity.AppException;
import com.citic.topview.common.system.vo.AppExceptionVO;
import com.citic.topview.system.dao.AppExceptionDao;
import com.citic.topview.system.persistence.CrudService;

@Service
@Transactional(readOnly = true)
public class AppExceptionService extends CrudService<AppExceptionDao, AppException>{

	public AppException get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(AppException appException) {
		return super.save(appException);
	}

	@Transactional(readOnly = false)
	public int update(AppException appException) {
		return super.update(appException);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<AppExceptionVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<AppExceptionVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}
}
