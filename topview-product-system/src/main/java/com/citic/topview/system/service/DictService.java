package com.citic.topview.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.system.entity.Dict;
import com.citic.topview.common.system.vo.DictVO;
import com.citic.topview.system.dao.DictDao;
import com.citic.topview.system.persistence.CrudService;

@Service
@Transactional(readOnly = true)
public class DictService extends CrudService<DictDao, Dict>{

	public Dict get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Dict dict) {
		return super.save(dict);
	}

	@Transactional(readOnly = false)
	public int update(Dict dict) {
		return super.update(dict);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<DictVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<DictVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}
}
