package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.bean.Query;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.system.entity.Tag;
import com.citic.topview.common.system.vo.TagVO;

@MyBatisDao
public interface TagDao extends CrudDao<Tag>{

	List<TagVO> list(Map<String, Object> params);
	
	List<TagVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);

	List<TagVO> listIds(Map<String, Object> params);

	List<TagVO> listById(Query query);
	
	List<TagVO> listByIN(Long[] ids);
}
