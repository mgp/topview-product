package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.system.entity.Office;
import com.citic.topview.common.system.vo.OfficeVO;

@MyBatisDao
public interface OfficeDao extends CrudDao<Office>{

	List<Office> findList(Map<String, Object> params);
	
	List<OfficeVO> list(Map<String, Object> params);
	
	List<OfficeVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);
	
	List<OfficeVO> listByIN(Long[] ids);

	int batchUpdate(Map<String, Object> params);
}
