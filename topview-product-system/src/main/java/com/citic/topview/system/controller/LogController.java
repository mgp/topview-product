package com.citic.topview.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.bean.Query;
import com.citic.topview.common.system.entity.Log;
import com.citic.topview.common.system.vo.LogVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.system.service.LogService;

@Controller
@RequestMapping("/a/log")
public class LogController {

	@Autowired
	private LogService logService;
	
	@ModelAttribute("log")
	public Log get(@RequestParam(required=false) Long id) {
		Log entity = null;
		if (id != null && id > 0L){
			entity = logService.get(id);
		}
		if (entity == null){
			entity = new Log();
		}
		return entity;
	}
	
	@RequiresPermissions("log:list")
	@RequestMapping("")
	public String log(Model model) {
		return "system/log/list";
	}
	
	@RequiresPermissions("log:list")
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<LogVO> logs = logService.listQuery(query);
		int total = logService.count(query);
		
		PageUtils pageUtil = new PageUtils(logs, total);
		return pageUtil;
	}
}
