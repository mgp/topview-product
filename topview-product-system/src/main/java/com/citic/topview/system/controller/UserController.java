package com.citic.topview.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.bean.Query;
import com.citic.topview.common.system.entity.User;
import com.citic.topview.common.system.vo.RoleVO;
import com.citic.topview.common.system.vo.UserVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.system.security.UserRealm;
import com.citic.topview.system.service.OfficeService;
import com.citic.topview.system.service.RoleService;
import com.citic.topview.system.service.SystemService;
import com.citic.topview.system.service.UserService;
import com.citic.topview.system.util.ShiroUtils;

@Controller
@RequestMapping("/a/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private OfficeService officeService;
	
	@Autowired
	private UserRealm userRealm;
	
	@ModelAttribute("user")
	public User get(@RequestParam(required=false) Long id) {
		User entity = null;
		if (id != null && id > 0L){
			entity = userService.get(id);
		}
		if (entity == null){
			entity = new User();
		}
		return entity;
	}
	
	@RequiresPermissions("user:list")
	@RequestMapping("")
	public String user(Model model) {
		return "system/user/list";
	}
	
	@Log("用户列表")
	@RequiresPermissions("user:list")
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		params.put("userType", "1");
		Query query = new Query(params);
		List<UserVO> users = userService.listQuery(query);
		int total = userService.count(query);
		
		PageUtils pageUtil = new PageUtils(users, total);
		return pageUtil;
	}
	
	@Log("用户添加")
	@RequiresPermissions("user:edit")
	@GetMapping("/add")
	public String add(Model model) {
		List<RoleVO> roles = roleService.list(new HashMap<String, Object>());
		model.addAttribute("roles", roles);
		
		return "system/user/add";
	}
	
	@Log("用户编辑")
	@RequiresPermissions("user:edit")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		User user = userService.get(id);
		
		if(user.getOfficeId() > 0L) {
			model.addAttribute("officeId", user.getOfficeId());
			model.addAttribute("officeName", officeService.get(user.getOfficeId()).getName());
		}
		
		List<RoleVO> roles = roleService.listByUserId(id);
		model.addAttribute("roles", roles);
		
		model.addAttribute("entity", user);
		return "system/user/edit";
	}
	
	@Log("个人信息")
	@RequiresPermissions("user:edit")
	@GetMapping("/introduction")
	public String introduction(Model model) {
		model.addAttribute("entity", ShiroUtils.getUser());
		return "system/user/introduction";
	}
	
	@Log("用户保存")
	@RequiresPermissions("user:edit")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("user") User user, Long[] roleIds) {
		user.setPassword(SystemService.entryptPassword(user.getPassword()));
		
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(userService.save(user, roleIds) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("用户更新")
	@RequiresPermissions("user:edit")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("user") User user, Long[] roleIds) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(userService.update(user, roleIds, userRealm) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("用户更新密码")
	@RequiresPermissions("user:edit")
	@PostMapping("/updatePassword")
	@ResponseBody
	public AjaxResultVO<Object> updatePassword(UserVO userVO) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		String message = userService.updatePassword(userVO);
		result.setResult(ResultVO.SUCCESS);
		result.setMessage(message);
		return result;
	}
	
	@Log("用户删除")
	@RequiresPermissions("user:edit")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(userService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}
