package com.citic.topview.system.gen;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;

import com.citic.topview.common.util.StringUtil;

public class GenerateUtil {

	private final static Set<String> set = new HashSet<String>();
	
	static {
		set.add("id");
		set.add("remark");
		set.add("createBy");
		set.add("createTime");
		set.add("modifiedBy");
		set.add("modifiedTime");
		set.add("delFlag");
	}
	
	
	public static List<String> getFields(List<String> fields) {
		List<String> list = new ArrayList<String>();
		for (String field : fields) {
			list.add(getField(new StringBuffer(field)).toString());
		}
		return list;
	}
	
	private static StringBuffer getField(StringBuffer buffer) {
		if(buffer.indexOf("_") == -1) {
			return buffer;
		} else {
			int index = buffer.indexOf("_");
			buffer.replace(index, index + 1, "");
			char ch = buffer.charAt(index);
			String upper = String.valueOf(new char[]{ch}).toUpperCase();
			buffer = buffer.replace(index, index + 1, upper);
			
			if(buffer.indexOf("_") != -1) {
				return getField(buffer);
			}
			
			return buffer;
		}
	}
	
	public static List<String> getTypes(List<String> types) {
		List<String> list = new ArrayList<String>();
		for (String type : types) {
			if(type.contains("bigint")) {
				list.add("Long");
			} else if(type.contains("varchar")) {
				list.add("String");
			} else if(type.contains("tinyint")) {
				list.add("Byte");
			} else if(type.contains("datetime")) {
				list.add("Date");
			} else if(type.contains("char")) {
				list.add("String");
			} else if(type.contains("char")) {
				list.add("String");
			} else if(type.contains("decimal")) {
				list.add("Integer");
			}
		}
		return list;
	}
	
	public static List<String[]> getColumns(List<String> types, List<String> fields, List<String> fields_, List<String> comments) {
		List<String[]> list = new ArrayList<String[]>();
		
		int index = 0;
		for (String field : fields) {
			if(!set.contains(field)) {
				list.add(new String[]{types.get(index), fields.get(index), comments.get(index), StringUtil.toUpperCaseFirstOne(fields.get(index)), fields_.get(index)});
			}
			
			index ++;
		}
		return list;
	}

	public static VelocityEngine getVelocityEngine() {
		Properties pro = new Properties();
        pro.put(Velocity.OUTPUT_ENCODING, "UTF-8");
        pro.put(Velocity.INPUT_ENCODING, "UTF-8");
        pro.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityEngine ve = new VelocityEngine(pro);		
        return ve;
	}

	public static void writeFile(VelocityContext context, Template t, String targetPath) {
        File file = new File(targetPath);
        if(!file.exists()) {
        	try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
        try {
            FileOutputStream outStream = new FileOutputStream(file);
            OutputStreamWriter writer = new OutputStreamWriter(outStream, "UTF-8");
            BufferedWriter sw = new BufferedWriter(writer);
            t.merge(context, sw);
            sw.flush();
            sw.close();
            outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
