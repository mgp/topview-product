package com.citic.topview.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.bean.Query;
import com.citic.topview.common.system.entity.Dict;
import com.citic.topview.common.system.vo.DictVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.system.service.DictService;

@Controller
@RequestMapping("/a/dict")
public class DictController {

	@Autowired
	private DictService dictService;
	
	@ModelAttribute("dict")
	public Dict get(@RequestParam(required=false) Long id) {
		Dict entity = null;
		if (id != null && id > 0L){
			entity = dictService.get(id);
		}
		if (entity == null){
			entity = new Dict();
		}
		return entity;
	}
	
	@RequiresPermissions("dict:list")
	@RequestMapping("")
	public String dict(Model model) {
		return "system/dict/list";
	}
	
	/*
	@RequestMapping("/list")
	@ResponseBody
	public List<DictVO> list(@RequestParam Map<String, Object> params) {
		List<DictVO> dicts = dictService.listQuery(params);
		return dicts;
	}
	*/
	
	@Log("字典列表")
	@RequiresPermissions("dict:list")
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<DictVO> dicts = dictService.listQuery(query);
		int total = dictService.count(query);
		
		PageUtils pageUtil = new PageUtils(dicts, total);
		return pageUtil;
	}
	
	@Log("字典添加")
	@RequiresPermissions("dict:list")
	@GetMapping("/add")
	public String add(Model model) {
		return "system/dict/add";
	}
	
	@Log("字典编辑")
	@RequiresPermissions("dict:list")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		Dict dict = dictService.get(id);
		model.addAttribute("entity", dict);
		return "system/dict/edit";
	}
	
	@Log("字典保存")
	@RequiresPermissions("dict:list")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("dict") Dict dict) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(dictService.save(dict) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("字典更新")
	@RequiresPermissions("dict:list")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("dict") Dict dict) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(dictService.update(dict) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("字典删除")
	@RequiresPermissions("dict:list")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(dictService.remove(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}
