package com.citic.topview.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.system.entity.Gen;
import com.citic.topview.common.system.vo.GenVO;
import com.citic.topview.system.dao.GenDao;
import com.citic.topview.system.persistence.CrudService;

@Service
@Transactional(readOnly = true)
public class GenService extends CrudService<GenDao, Gen>{

	public Gen get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Gen gen) {
		return super.save(gen);
	}

	@Transactional(readOnly = false)
	public int update(Gen gen) {
		return super.update(gen);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<GenVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<GenVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Map<String, Object> params) {
		return dao.count(params);
	}
}
