package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.system.entity.TagOffice;
import com.citic.topview.common.system.vo.TagOfficeVO;
import com.citic.topview.common.system.vo.TagVO;

@MyBatisDao
public interface TagOfficeDao {
	
	public TagOffice get(Long id);
	
	public int save(TagOffice entity);
	
	public int batchSave(List<TagOffice> list);
	
	public int update(TagOffice entity);
	
	public int remove(Long id);
	
	public int removeByTagId(Long tagId);
	
	public List<TagOfficeVO> list(Map<String, Object> params);

	public int removeByVO(TagVO tagVO);

}