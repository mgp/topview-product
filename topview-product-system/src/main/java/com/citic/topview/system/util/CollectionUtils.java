package com.citic.topview.system.util;

import java.util.List;

public class CollectionUtils {

	public static boolean isEqual(List<Long> list1, List<Long> list2) {
		if(org.apache.commons.collections.CollectionUtils.isEmpty(list1) || org.apache.commons.collections.CollectionUtils.isEmpty(list2)) {
			throw new RuntimeException("Collections are not allowed to be empty and cannot be zero in length");
		}
		
		if(list1.size() == list2.size()) {
			boolean b = true;
			for (Long long1 : list1) {
				if(!list2.contains(long1)) {
					b = false;
				}
			}
			return b;
		}
		
		return false;
	}

}
