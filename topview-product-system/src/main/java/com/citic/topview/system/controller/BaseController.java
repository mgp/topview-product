package com.citic.topview.system.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citic.topview.common.vo.AppResultVO;

public abstract class BaseController {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	public AppResultVO<Object> setResult(String result, String message, String remark) {
		AppResultVO<Object> mobileResultVO = new AppResultVO<Object>();
		mobileResultVO.setResult(result);
		mobileResultVO.setMessage(message);
		mobileResultVO.setRemark(remark);
		return mobileResultVO;
	}
}
