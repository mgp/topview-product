package com.citic.topview.system.dao;

import java.util.List;
import java.util.Map;

import com.citic.topview.common.annotation.MyBatisDao;
import com.citic.topview.common.persistence.CrudDao;
import com.citic.topview.common.system.entity.Role;
import com.citic.topview.common.system.vo.RoleVO;

@MyBatisDao
public interface RoleDao extends CrudDao<Role>{

	List<RoleVO> list(Map<String, Object> params);
	
	List<RoleVO> listQuery(Map<String, Object> params);

	int count(Map<String, Object> params);

	List<RoleVO> listByUserId(Long userId);
}
