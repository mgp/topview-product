package com.citic.topview.system.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.bean.Query;
import com.citic.topview.common.bean.Tree;
import com.citic.topview.common.system.entity.Menu;
import com.citic.topview.common.system.vo.MenuVO;
import com.citic.topview.common.util.BuildTree;
import com.citic.topview.system.dao.MenuDao;
import com.citic.topview.system.dao.RoleMenuDao;
import com.citic.topview.system.persistence.CrudService;
import com.citic.topview.system.util.ListHelper;

@Service
@Transactional(readOnly = true)
public class MenuService extends CrudService<MenuDao, Menu>{

	@Autowired
	RoleMenuDao roleMenuDao;
	
	public Menu get(Long id) {
		return super.get(id);
	}

	@Transactional(readOnly = false)
	public int save(Menu menu) {
		return super.save(menu);
	}

	@Transactional(readOnly = false)
	public int update(Menu menu) {
		return super.update(menu);
	}

	@Transactional(readOnly = false)
	public int remove(Long id) {
		return super.remove(id);
	}

	@Transactional(readOnly = false)
	public int deleteByDelFlag(Long id) {
		return super.deleteByDelFlag(id);
	}

	@Transactional(readOnly = false)
	public int batchRemove(Long[] ids) {
		return super.batchRemove(ids);
	}
	
	public List<MenuVO> list(Map<String, Object> params) {
		return dao.list(params);
	}
	
	public List<MenuVO> listQuery(Map<String, Object> params) {
		return dao.listQuery(params);
	}
	
	public int count(Query query) {
		return dao.count(query);
	}
	
	public Tree<MenuVO> getTree() {
		List<Tree<MenuVO>> trees = new ArrayList<Tree<MenuVO>>();
		List<MenuVO> list = dao.list(new HashMap<>(16));
		for (MenuVO menuVO : list) {
			Tree<MenuVO> tree = new Tree<MenuVO>();
			tree.setId(menuVO.getId());
			tree.setParentId(menuVO.getParentId());
			tree.setText(menuVO.getName());
			trees.add(tree);
		}
		// 默认顶级菜单为０，根据数据库实际情况调整
		Tree<MenuVO> t = BuildTree.buildMenu(trees);
		return t;
	}

	public Tree<MenuVO> getTree(Long roleId) {
		List<MenuVO> list = dao.list(new HashMap<String, Object>(16));
		
		List<Long> menuIds = roleMenuDao.listMenuIdByRoleId(roleId);
		List<Long> temp = menuIds;
		for (MenuVO menu : list) {
			if (temp.contains(menu.getParentId())) {
				menuIds.remove(menu.getParentId());
			}
		}
		
		List<Tree<MenuVO>> trees = new ArrayList<Tree<MenuVO>>();
		for (MenuVO menuVO : list) {
			Tree<MenuVO> tree = new Tree<MenuVO>();
			tree.setId(menuVO.getId());
			tree.setParentId(menuVO.getParentId());
			tree.setText(menuVO.getName());
			
			Map<String, Object> state = new HashMap<>(16);
			if (menuIds.contains(menuVO.getId())) {
				state.put("selected", true);
			} else {
				state.put("selected", false);
			}
			tree.setState(state);
			
			trees.add(tree);
		}
		// 默认顶级菜单为０，根据数据库实际情况调整
		Tree<MenuVO> t = BuildTree.buildMenu(trees);
		return t;
	}

	public List<MenuVO> listByRoleIds(List<Long> ids) {
		return dao.listByRoleIds(ids);
	}

	public int removes(Long id) {
		int i = roleMenuDao.removeByMenuId(id);
		int j = remove(id);
		return i + j;
	}

	public List<MenuVO> getTopList() {
		List<MenuVO> list = list(new HashMap<String, Object>());
		for (MenuVO menuVO : list) {
			if(StringUtils.isBlank(menuVO.getHref())) {
				menuVO.setHref("#");
			}
			ListHelper.processListToTree(menuVO, list);
		}
		
		List<MenuVO> topList = new ArrayList<>();
		for (MenuVO menuVO : list) {
			if(menuVO.getParentId().equals(0L)) {
				topList.add(menuVO);
			}
		}
		
		return topList;
	}
}
