package com.citic.topview.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.citic.topview.common.system.entity.TagOffice;
import com.citic.topview.common.system.vo.TagOfficeVO;
import com.citic.topview.system.dao.TagOfficeDao;

@Service
@Transactional(readOnly = true)
public class TagOfficeService {

	@Autowired
	TagOfficeDao tagOfficeDao;
	
	public TagOffice get(Long id) {
		return tagOfficeDao.get(id);
	}
	
	@Transactional(readOnly = false)
	public int save(TagOffice entity) {
		return tagOfficeDao.save(entity);
	}
	
	@Transactional(readOnly = false)
	public void batchSave(List<TagOffice> list) {
		tagOfficeDao.batchSave(list);
	}
	
	@Transactional(readOnly = false)
	public int update(TagOffice entity) {
		return tagOfficeDao.save(entity);
	}
	
	@Transactional(readOnly = false)
	public int remove(Long id) {
		return tagOfficeDao.remove(id);
	}
	
	public List<TagOfficeVO> list(Map<String, Object> params) {
		return tagOfficeDao.list(params);
	}
}
