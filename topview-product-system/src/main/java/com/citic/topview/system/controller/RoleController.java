package com.citic.topview.system.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.citic.topview.common.annotation.Log;
import com.citic.topview.common.bean.Query;
import com.citic.topview.common.system.entity.Role;
import com.citic.topview.common.system.vo.RoleVO;
import com.citic.topview.common.vo.AjaxResultVO;
import com.citic.topview.common.vo.PageUtils;
import com.citic.topview.common.vo.ResultVO;
import com.citic.topview.system.security.UserRealm;
import com.citic.topview.system.service.RoleService;

@Controller
@RequestMapping("/a/role")
public class RoleController {

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private UserRealm userRealm;
	
	@ModelAttribute("role")
	public Role get(@RequestParam(required=false) Long id) {
		Role entity = null;
		if (id != null && id > 0L){
			entity = roleService.get(id);
		}
		if (entity == null){
			entity = new Role();
		}
		return entity;
	}
	
	@RequiresPermissions("role:list")
	@RequestMapping("")
	public String role(Model model) {
		return "system/role/list";
	}
	
	@Log("角色列表")
	@RequiresPermissions("role:list")
	@GetMapping("/list")
	@ResponseBody
	PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<RoleVO> roles = roleService.listQuery(query);
		int total = roleService.count(query);
		
		PageUtils pageUtil = new PageUtils(roles, total);
		return pageUtil;
	}
	
	@Log("角色添加")
	@RequiresPermissions("role:edit")
	public String add(Model model) {
		return "system/role/add";
	}
	
	@Log("角色编辑")
	@RequiresPermissions("role:edit")
	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") Long id) {
		Role role = roleService.get(id);
		model.addAttribute("entity", role);
		return "system/role/edit";
	}
	
	@Log("角色保存")
	@RequiresPermissions("role:edit")
	@PostMapping("/save")
	@ResponseBody
	public AjaxResultVO<Object> save(@ModelAttribute("role") Role role, Long[] menuIds) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(roleService.save(role, menuIds) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("角色更新")
	@RequiresPermissions("role:edit")
	@PostMapping("/update")
	@ResponseBody
	public AjaxResultVO<Object> update(@ModelAttribute("role") Role role, Long[] menuIds) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(roleService.update(role, menuIds) > 0) {
			userRealm.clearAuthz();
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
	
	@Log("角色删除")
	@RequiresPermissions("role:edit")
	@PostMapping("/remove")
	@ResponseBody
	public AjaxResultVO<Object> remove(Long id) {
		AjaxResultVO<Object> result = new AjaxResultVO<Object>();
		if(roleService.removeAll(id) > 0) {
			result.setResult(ResultVO.SUCCESS);
			return result;
		} else {
			result.setResult(ResultVO.ERROR);
			return result;
		}
	}
}
